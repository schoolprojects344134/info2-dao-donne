package fr.uge.ugegreed.network;

public enum Operation {
    NONE, CONNECTION, UPDATE_NETWORK_WEIGHT, UPDATE_ROUTE_TABLE, REQUEST_CALCULATION
}
