package fr.uge.ugegreed.network;

public enum ContextState {
    CONNECTING, WAITING, RECONNECTION
}
