package fr.uge.ugegreed.network;

public enum NetworkState {
    LOCAL, CONNECTED, RECONNECTION
}
