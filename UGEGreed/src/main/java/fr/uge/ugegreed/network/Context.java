package fr.uge.ugegreed.network;

import fr.uge.ugegreed.packet.*;
import fr.uge.ugegreed.packet.writer.Writer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.logging.Logger;

public class Context {
    public static final int CONTEXT_BUFFER_SIZE = 10_000;
    private static Logger logger = Logger.getLogger(Context.class.getName());
    private final ByteBuffer bufferIn = ByteBuffer.allocate(CONTEXT_BUFFER_SIZE);
    private final ByteBuffer bufferOut = ByteBuffer.allocate(CONTEXT_BUFFER_SIZE);
    private final SelectionKey key;
    private final SocketChannel sc;
    private final ArrayDeque<Writer> senderQueue = new ArrayDeque<>();
    private final InetSocketAddress address;
    private boolean closed = false;
    private final PacketReader packetReader;
    private final PacketConsumer packetProcessor;


    Context(SelectionKey key, InetSocketAddress address, PacketConsumer packetProcessor, PacketReader packetReader) throws IOException {
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.packetProcessor = packetProcessor;
        this.address = address;
        this.packetReader = packetReader;
    }

    /**
     * Process the content of bufferIn
     *
     * The convention is that bufferIn is in write-mode before the call to process and
     * after the call
     *
     */
    private void processIn() {
            for (;;) {
                var status = packetReader.process(bufferIn);
                switch (status) {
                    case DONE:
                        var packet = packetReader.get();
                        //logger.info(address + " received packet: " + packet);
                        packetProcessor.process(this, packet);
                        packetReader.reset();
                        break;
                    case REFILL:
                        //System.out.println(address + " REFILL :) " + bufferIn.position());
                        return;
                    case ERROR:
                        silentlyClose();
                        return;
                }
            }
    }

    /**
     * Add a message to the message queue, tries to fill bufferOut and updateInterestOps
     *
     * @param packet
     */
    public void queuePacket(Packet packet) {
        senderQueue.add(packet.createWriter());
        //System.out.println("Queued packet " + packet);
        processOut();
        updateInterestOps();
    }

    /*
    public ContextState getState(){
        return state;
    }

    public void setState(ContextState contextState){
        state = contextState;
    }
     */

    public InetSocketAddress getAddress(){
        return address;
    }

    public void closeConnection(){
        closed = true;
    }

    /**
     * Try to fill bufferOut from the message queue
     *
     */
    private void processOut() {
            if(senderQueue.isEmpty()){
                return;
            }
            var nextWriter = senderQueue.peek();
            if(nextWriter.process(bufferOut) == Writer.ProcessStatus.DONE){
                nextWriter.reset(); // Need to check if this reset is required may be for broadcast we will send the same writer to everyone
                senderQueue.remove();
            }
    }

    /**
     * Update the interestOps of the key looking only at values of the boolean
     * closed and of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call to
     * updateInterestOps and after the call. Also it is assumed that process has
     * been be called just before updateInterestOps.
     */
    private void updateInterestOps() {
        var newInterestOps = 0;
        if(!closed && bufferIn.hasRemaining()){
            newInterestOps |= SelectionKey.OP_READ;
        }

        if(bufferOut.position() != 0){
            newInterestOps |= SelectionKey.OP_WRITE;
        }

        if(newInterestOps == 0){
            silentlyClose();
            return;
        }
        key.interestOps(newInterestOps);
    }

    /**
     * Silently close the context connection
     */
    private void silentlyClose() {
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call to
     * doRead and after the call
     *
     */
    public void doRead() throws IOException {
        if(sc.read(bufferIn) == -1){
            closed = true;
        }
        processIn();
        updateInterestOps();
    }

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call to
     * doWrite and after the call
     *
     * @throws IOException
     */
    public void doWrite() throws IOException {
        bufferOut.flip();
        sc.write(bufferOut);
        bufferOut.compact();
        processOut();
        updateInterestOps();
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Context context){
            // A context is equals to another context only if they have the same address
            return context.getAddress().equals(address);
        }
        return false;
    }

    @Override
    public int hashCode(){
        return address.hashCode();
    }

}
