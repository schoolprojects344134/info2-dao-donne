package fr.uge.ugegreed.network;

import fr.uge.ugegreed.operation.CalculationIdentifierResultOperation;
import fr.uge.ugegreed.operation.CalculationRequestOperation;
import fr.uge.ugegreed.operation.InterruptCalculationOperation;
import fr.uge.ugegreed.operation.LocalCalculationResultOperation;
import fr.uge.ugegreed.packet.*;

import java.io.IOException;
import java.util.logging.Logger;

public class ParentProcessor {
    private static final Logger logger = Logger.getLogger(ParentProcessor.class.getName());
    private final NetworkProcessor networkProcessor;

    public ParentProcessor(NetworkProcessor networkProcessor) {
        this.networkProcessor = networkProcessor;
    }

    public void processConnectionRefusedPacket(ConnectionRefusedPacket packet, Context sender) {
        // TODO close connection when client receive this request but if we change the rfc we can also simply close the connection on the server
        var pendingConnection = networkProcessor.removePendingConnection(packet.destinationAddress());
        if(pendingConnection != null){
            pendingConnection.queuePacket(packet);
        }
    }

    public void processConnectionAcceptPacket(ConnectionAcceptPacket packet, Context sender) {
        // Check if application is the destination of the packet
        if(networkProcessor.isApplicationReceiver(packet.destinationAddress())){
            // As the connection is accepted then update network weight and route table
            networkProcessor.sendPacketToParent(new RouteTableAddPathPacket(networkProcessor.applicationAddress()));
            networkProcessor.sendPacketToParent(new IncrementWeightPacket());
            return;
        }
        // Check if there is a pending connection then send it packet
        var pendingConnection = networkProcessor.removePendingConnection(packet.destinationAddress());
        if(pendingConnection != null){
            pendingConnection.queuePacket(packet);
            return;
        }
        // Forward the packet to the child who know the destination client
        var destination = networkProcessor.getRoute(packet.destinationAddress());
        if(destination == null){
            return;
        }
        destination.queuePacket(packet);
    }

    public void processNetworkWeightResponsePacket(NetworkWeightResponsePacket packet, Context sender) {
        // Forward the packet to the child who know the destination client
        var destination = networkProcessor.getRoute(packet.destinationAddress());
        if(destination != null){
            destination.queuePacket(packet);
            return;
        }
        // Dispatch next pending calculation
        networkProcessor.dispatchCalculation(packet.networkWeight());
    }

    public void processCalculationRequestPacket(CalculationRequestPacket packet, Context sender) {
        // Add the calculation request received from the parent to the calculation queue
        var operation = new CalculationRequestOperation(packet.address(),packet.jarUrl(),packet.className(),packet.calculationId(),packet.startRange(),packet.endRange());
        networkProcessor.queueCalculation(sender.getAddress(), operation);
        // Send a network weight request to process calculation dispatch when the response is received
        networkProcessor.sendPacketToParent(new NetworkWeightRequestPacket(networkProcessor.applicationAddress()));
    }

    public void processCalculationResultPacket(CalculationResultPacket packet, Context sender) {
        // Forward the packet to the child who know the destination client
        var destination = networkProcessor.getRoute(packet.destinationAddress());
        if(destination != null){
            destination.queuePacket(packet);
            return;
        }

        // If application is the destination of the packet then save calculation results (It can happen that address are not the same after application reconnection)
        networkProcessor.saveCalculationResult(new LocalCalculationResultOperation(packet.calculationId(), packet.calculationResponses()));
    }

    public void processCalculationInterruptPacket(CalculationInterruptPacket packet, Context sender) {
        // Remove all calulations from packet source address
        networkProcessor.interruptNetworkCalculationFromSender(packet.sourceAddress(), sender);
    }

    public void processReconnectionRequestPacket(ReconnectionRequestPacket packet, Context sender) {
        networkProcessor.setState(NetworkState.RECONNECTION);
        // Parent want to disconnect
        var newDestinationParentAddress = packet.newParentAddress();
        try {
            networkProcessor.reconnection(newDestinationParentAddress);
        } catch (IOException e) {
            logger.severe("Reconnection to another parent failed: " + e);
        }
    }

    public void processReconnectionAcceptPacket(ReconnectionAcceptPacket packet, Context sender) {
        networkProcessor.setState(NetworkState.CONNECTED);
        networkProcessor.sendPacketToParent(new RouteTableAddPathPacket(networkProcessor.applicationAddress()));
    }

    public void processCalculationIdentifierResultPacket(CalculationIdentifierResultPacket packet, Context sender) {
        // Check route for the packet
        var destination = networkProcessor.getRoute(packet.destinationAddress());
        if(destination != null){
            destination.queuePacket(packet);
            return;
        }
        // Process if application is destination
        if(networkProcessor.isApplicationReceiver(packet.destinationAddress())){
            networkProcessor.sendOperation(new CalculationIdentifierResultOperation(packet.calculationIdentifier()));
        }
    }

}
