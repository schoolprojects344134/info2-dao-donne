package fr.uge.ugegreed.network;

import fr.uge.ugegreed.operation.CalculationRequestOperation;
import fr.uge.ugegreed.operation.InterruptCalculationOperation;
import fr.uge.ugegreed.operation.LocalCalculationResultOperation;
import fr.uge.ugegreed.packet.*;

import java.util.logging.Logger;

public class ChildProcessor {
    private static final Logger logger = Logger.getLogger(ChildProcessor.class.getName());
    private final NetworkProcessor networkProcessor;

    public ChildProcessor(NetworkProcessor networkProcessor){
        this.networkProcessor = networkProcessor;
    }

    public void processConnectionPacket(ConnectionPacket packet, Context sender){

        // If application is disconnected then refuse the connection //TODO check if necessary at the end
        /*
        if(networkProcessor.getState() == NetworkState.DISCONNECTED){
            sender.queuePacket(new ConnectionRefusedPacket(packet.sourceAddress()));
            return;
        }
        */

        // Set network state as connected after the first client connection on the application server
        /*
        if(networkProcessor.getState() != NetworkState.CONNECTED){
            networkProcessor.setState(NetworkState.CONNECTED);
        }
         */

        // If root check client connection otherwise send the request to the parent
        if(networkProcessor.isApplicationRoot()){
            // Check if the network already contains the address of the client which request the connection to the network
            var destination = networkProcessor.getRoute(packet.sourceAddress());
            if(destination != null && !networkProcessor.isApplicationReceiver(packet.sourceAddress())){
                sender.queuePacket(new ConnectionRefusedPacket(packet.sourceAddress()));
                return;
            }
            // If destination not found then the connection is accepted
            sender.queuePacket(new ConnectionAcceptPacket(packet.sourceAddress()));
            return;
        }
        // Add pending connection (as route table are not yet ready, we need to remind who is the sender)
        if(!networkProcessor.addPendingConnection(packet.sourceAddress(), sender)){
            // If there is already a pending connection for this client then refuse connection
            sender.queuePacket(new ConnectionRefusedPacket(packet.sourceAddress()));
            return;
        }
        // Forward the packet to the parent
        networkProcessor.sendPacketToParent(packet);
    }

    public void processRouteTableAddPathPacket(RouteTableAddPathPacket packet, Context sender){
        // Add route to the client
        networkProcessor.addRoute(packet.address(),sender);
        // Forward the packet to the parent
        if(!networkProcessor.isApplicationRoot()){
            networkProcessor.sendPacketToParent(packet);
        }
    }

    public void processRouteTableRemovePathPacket(RouteTableRemovePathPacket packet, Context sender){
        // Remove route to the client
        networkProcessor.removeRoute(packet.address());
        // Forward the packet to the parent
        if(!networkProcessor.isApplicationRoot()){
            networkProcessor.sendPacketToParent(packet);
        }
    }

    public void processIncrementWeightPacket(IncrementWeightPacket packet, Context sender){
        // Increase the network weight of the child
        networkProcessor.increaseWeight(sender);
        // If not root forward the packet to the parent
        if(!networkProcessor.isApplicationRoot()){
            networkProcessor.sendPacketToParent(packet);
        }
    }

    public void processDecrementWeightPacket(DecrementWeightPacket packet, Context sender){
        // Decrease the network weight of the child
        networkProcessor.decreaseWeight(sender);
        // If not root forward the packet to the parent
        if(!networkProcessor.isApplicationRoot()){
            networkProcessor.sendPacketToParent(packet);
        }
    }

    public void processNetworkWeightRequestPacket(NetworkWeightRequestPacket packet, Context sender){
        // If it's root then send response to the client with the network weight
        if(networkProcessor.isApplicationRoot()){
            sender.queuePacket(new NetworkWeightResponsePacket(packet.sourceAddress(), networkProcessor.getNodeWeight()));
            return;
        }
        // Otherwise give the packet to the parent
        networkProcessor.sendPacketToParent(packet);
    }

    /*
    public void processNetworkWeightResponsePacket(NetworkWeightResponsePacket packet, Context sender){
        // Root should not receive this but for safety it's better to make a check
        if(isRoot){
            return;
        }
        // Dispatch calculation received
        networkProcessor.dispatchCalculation(packet.networkWeight());
    }
     */

    public void processCalculationRequestPacket(CalculationRequestPacket packet, Context sender){
        var calculationOperation = new CalculationRequestOperation(packet.address(),packet.jarUrl(),packet.className(),packet.calculationId(),packet.startRange(),packet.endRange());
        // Add request calculation to the calculation queue
        networkProcessor.queueCalculation(sender.getAddress(), calculationOperation);

        //If is root then directly dispatch calculation
        if(networkProcessor.isApplicationRoot()){
            networkProcessor.dispatchCalculation(networkProcessor.getNodeWeight());
            return;
        }

        // If not root then request the network weight that was required for the calculation dispatching
        networkProcessor.sendPacketToParent(new NetworkWeightRequestPacket(networkProcessor.applicationAddress()));
    }

    public void processCalculationResultPacket(CalculationResultPacket packet, Context sender){
        var destinationAddress = packet.destinationAddress();
        // Get route to the destination
        var destinationContext = networkProcessor.getRoute(destinationAddress);
        if(destinationContext != null){
            destinationContext.queuePacket(packet);
            return;
        }
        // If the application is the destination of the calculation results then save it
        if(networkProcessor.isApplicationReceiver(destinationAddress)){
            networkProcessor.saveCalculationResult(new LocalCalculationResultOperation(packet.calculationId(), packet.calculationResponses()));
            return;
        }
        // If rout was not found and the receiver is not the application then pass the packet to the parent
        if(!networkProcessor.isApplicationRoot()){
            networkProcessor.sendPacketToParent(packet);
        }
    }

    public void processCalculationInterruptPacket(CalculationInterruptPacket packet, Context sender){
        // Process calculation interrupt operation
        networkProcessor.interruptNetworkCalculationFromSender(packet.sourceAddress(), sender);
    }

    public void processReconnectionPacket(ReconnectionPacket packet, Context sender){
        var clientAddresses = packet.childrenAddresses();
        // Remove all previous route
        clientAddresses.forEach(networkProcessor::removeRoute);
        // Set all new route (IMPORTANT NOTE: it also redirected the previous application address to the same application)
        clientAddresses.forEach(address -> networkProcessor.addRoute(address, sender));
        // Send reconnection accept packet
        sender.queuePacket(new ReconnectionAcceptPacket());
    }

    public void processCalculationIdentifierRequest(CalculationIdentifierRequestPacket packet, Context sender){
        // If root then send calculation id generated
        if(networkProcessor.isApplicationRoot()){
            sender.queuePacket(new CalculationIdentifierResultPacket(packet.sourceAddress(), networkProcessor.generateCalculationId()));
            return;
        }
        // Or forward packet to the parent
        networkProcessor.sendPacketToParent(packet);
    }

    public void processCalculationIdentifierReleasePacket(CalculationIdentifierReleasePacket packet, Context sender){
        // If root then release calculation id
        if(networkProcessor.isApplicationRoot()){
            networkProcessor.releaseId(packet.calculationIdentifier());
            return;
        }
        // Or forward packet to the parent
        networkProcessor.sendPacketToParent(packet);
    }

}
