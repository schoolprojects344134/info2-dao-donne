package fr.uge.ugegreed.network;

import fr.uge.ugegreed.operation.*;
import fr.uge.ugegreed.packet.*;

public class OperationProcessor {
    private final NetworkProcessor networkProcessor;

    public OperationProcessor(NetworkProcessor networkProcessor){
        this.networkProcessor = networkProcessor;
    }
    public void processNewCalculationOperation(NewCalculationOperation newCalculationOperation){
        //If the application don't have children and no parent then process calculation without dispatching
        if(networkProcessor.isApplicationRoot() && networkProcessor.childCount() == 0){
            networkProcessor.sendOperation(newCalculationOperation);
            return;
        }

        // Create calculation operation
        var operation = new CalculationRequestOperation(
                networkProcessor.applicationAddress(),
                newCalculationOperation.jarUrl(),
                newCalculationOperation.className(),
                newCalculationOperation.calculationId(),
                newCalculationOperation.startRange(),
                newCalculationOperation.endRange()
        );

        // Queue operation since the network weight is not received
        networkProcessor.queueCalculation(networkProcessor.applicationAddress(), operation);

        // If root then directly dispatch calculation
        if(networkProcessor.isApplicationRoot()){
            networkProcessor.dispatchCalculation(networkProcessor.getNodeWeight());
            return;
        }

        // Otherwise send a network weight request
        networkProcessor.sendPacketToParent(new NetworkWeightRequestPacket(networkProcessor.applicationAddress()));
    }

    // Remote calculation request
    public void processCalculationOperation(CalculationRequestOperation calculationRequestOperation){
        networkProcessor.queueCalculation(networkProcessor.applicationAddress(), calculationRequestOperation);

        // If is root directly dispatch calculation
        if(networkProcessor.isApplicationRoot()){
            networkProcessor.dispatchCalculation(networkProcessor.getNodeWeight());
            return;
        }

        // Otherwise send a network weight request
        networkProcessor.sendPacketToParent(new NetworkWeightRequestPacket(networkProcessor.applicationAddress()));
    }

    public void processDisconnectOperation(DisconnectOperation disconnectOperation){
        if(networkProcessor.isApplicationRoot()){
            // TODO manage root disconnection when no children only
            if(networkProcessor.childCount() == 0){
                networkProcessor.disconnection();
                return;
            }
        }

        // Refuse new calculation and decrement weight of the network
        networkProcessor.disableCalculation();
        networkProcessor.sendPacketToParent(new DecrementWeightPacket());

        // Interrupt network calculation
        networkProcessor.interruptNetworkCalculation();

        //Reconnect children
        networkProcessor.requestChildrenReconnection();

        // Remove application address from all network parents route table
        networkProcessor.sendPacketToParent(new RouteTableRemovePathPacket(networkProcessor.applicationAddress()));

        // Need to wait for disptaching, no enough time//
        networkProcessor.sendOperation(new DispatchCalculationOperation(networkProcessor.applicationAddress()));
    }

    public void processCalculationResultOperation(CalculationResultOperation calculationResultOperation){
        // Check if calculation results are associated with a local or a remote calculation
        switch (calculationResultOperation){
            case RemoteCalculationResultOperation op -> {
                switch (op.calculableOperation()){
                    case CalculationRequestOperation opRequest -> {
                        // If application is receiver of calculation results
                        if(networkProcessor.isApplicationReceiver(opRequest.responseDestinationAddress())){
                            networkProcessor.saveCalculationResult(new LocalCalculationResultOperation(op.calculationId(),op.responses()));
                            return;
                        }
                        // Create response packet
                        var resultPacket = new CalculationResultPacket(opRequest.responseDestinationAddress(), networkProcessor.applicationAddress(), opRequest.calculationId(), op.responses());
                        // Route packet to destination child
                        var destination = networkProcessor.getRoute(opRequest.responseDestinationAddress());
                        if(destination != null){
                            destination.queuePacket(resultPacket);
                            return;
                        }
                        // If route not found forward the packet to the parent
                        networkProcessor.sendPacketToParent(resultPacket);
                    }
                    case NewCalculationOperation opRequest -> throw new UnsupportedOperationException();
                    default -> throw new IllegalStateException("Unexpected value: " + op.calculableOperation());
                }
            }
            case LocalCalculationResultOperation op -> networkProcessor.saveCalculationResult(op);
            default -> throw new IllegalStateException("Unexpected value: " + calculationResultOperation);
        }
    }

    public void processUpdateApplicationCalculation(UpdateApplicationCalculation updateApplicationCalculation){
        if(updateApplicationCalculation.activated()){
            networkProcessor.enableCalculation();
        }else {
            networkProcessor.disableCalculation();
        }
    }

    public void processCalculationIdentifierRequestOperation(CalculationIdentifierRequestOperation calculationIdentifierRequestOperation){
        // If root directly generate a new calculation id
        if(networkProcessor.isApplicationRoot()){
            networkProcessor.sendOperation(new CalculationIdentifierResultOperation(networkProcessor.generateCalculationId()));
            return;
        }
        // Forward the packet to the parent
        networkProcessor.sendPacketToParent(new CalculationIdentifierRequestPacket(networkProcessor.applicationAddress()));
    }

    public void processCalculationIdentifierReleaseOperation(CalculationIdentifierReleaseOperation calculationIdentifierReleaseOperation){
        // If root directly release calculation id
        if(networkProcessor.isApplicationRoot()){
            networkProcessor.releaseId(calculationIdentifierReleaseOperation.calculationId());
            return;
        }
        // Forward the packet to the parent
        networkProcessor.sendPacketToParent(new CalculationIdentifierReleasePacket(calculationIdentifierReleaseOperation.calculationId()));
    }

    public void processShutdownResultOperation(ShutdownResultOperation shutdownResultOperation){
        networkProcessor.disconnection();
    }

}
