package fr.uge.ugegreed.network;

import fr.uge.ugegreed.operation.Operation;
import fr.uge.ugegreed.operation.CalculationRequestOperation;
import fr.uge.ugegreed.packet.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NetworkManager {
    private static final Logger logger = Logger.getLogger(NetworkManager.class.getName());
    private final Object lock = new Object();
    private final ServerSocketChannel serverSocketChannel;
    private final Selector selector;
    private final boolean isRoot;
    private Context parentContext;
    private final ArrayDeque<Operation> operationQueue = new ArrayDeque<>();
    private final PacketReader packetReader = new PacketReader();
    private final NetworkProcessor networkProcessor;
    private boolean closed;
    private final SelectionKey serverKey;

    /**
     * Create a new network manager
     *
     * @param port
     * @param parentAddress
     * @param operationProcessor
     * @throws IOException throw IOException if server failed to start
     */
    public NetworkManager(int port, InetSocketAddress parentAddress, Consumer<Operation> operationProcessor) throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        selector = Selector.open();
        this.isRoot = parentAddress == null;

        this.networkProcessor = new NetworkProcessor(this, operationProcessor);

        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.bind(new InetSocketAddress(port));
        this.serverKey = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        // Configure the client connection to the parent if it's not the root
        if(!isRoot){
            // Method in constructor (I don't like that but don't worry :))
            connectionToParent(parentAddress);
        }else{
            // Configure the server when it's root because it doesn't need to wait for the parent context to be connected to a server
            this.parentContext = null;
        }

        // Start thread which loop on selection key
        Thread.ofPlatform().start(() -> {
            try {
                launch();
            } catch (IOException e) {
                logger.severe("The application server encountered a fatal IOException when looping on selection key: " + e);
            }
        });
    }

    /**
     * Process connection to the parent application
     * @param parentAddress parent address
     * @throws IOException thrown if it failed to connect tot the parent
     */
    public void connectionToParent(InetSocketAddress parentAddress) throws IOException {
        var clientSocketChannel = SocketChannel.open();
        clientSocketChannel.configureBlocking(false);
        var key = clientSocketChannel.register(selector, SelectionKey.OP_CONNECT);
        this.parentContext = new Context(key, parentAddress, networkProcessor::processParentPacket, packetReader);
        key.attach(parentContext);
        this.networkProcessor.setParent(this.parentContext);
        clientSocketChannel.connect(parentAddress);
    }

    public void disconnect(){
        synchronized (lock){
            closed = true;
            selector.wakeup();
        }
    }

    /**
     * Process the next operation contained in the operation queue
     */
    private void processOperation(){
        synchronized(lock) {
            while(!operationQueue.isEmpty()){
                var operation = operationQueue.poll();
                networkProcessor.processOperation(operation);
            }
        }
    }

    /**
     * Queue network operation
     * @param operation operation request
     */
    public void queueOperation(Operation operation){
        synchronized(lock) {
            operationQueue.add(operation);
            selector.wakeup();
        }
    }

    public void launch() throws IOException {
        while (!Thread.interrupted()) {
            try {
                selector.select(this::treatKey);
                processOperation();
                synchronized (lock){
                    if(closed){
                        silentlyClose(serverKey);
                        return;
                    }
                }
            } catch (UncheckedIOException tunneled) {
                throw tunneled.getCause();
            }
        }
    }

    /**
     * Treat key of the selector and process corresponding context action
     * @param key selection key
     */
    private void treatKey(SelectionKey key) {
        try {
            if (key.isValid() && key.isAcceptable()) {
                doAccept(key);
            }
        } catch (IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }
        try {
            if (key.isValid() && key.isConnectable()) {
                doConnect(key);
                //((Context) key.attachment()).doConnect();
            }
            if (key.isValid() && key.isWritable()) {
                ((Context) key.attachment()).doWrite();
            }
            if (key.isValid() && key.isReadable()) {
                ((Context) key.attachment()).doRead();
            }
        } catch (IOException e) {
            logger.log(Level.INFO, "Client connection closed with client due to IOException", e);
            silentlyClose(key);
        }
    }



    /**
     * Accept new client on the application server
     * @param key selection key
     * @throws IOException thrown if there is an error while connection a client
     */
    private void doAccept(SelectionKey key) throws IOException {
        var sc = serverSocketChannel.accept();
        if(sc == null){
            logger.warning("The selector lies !");
            return;
        }
        try{
            sc.configureBlocking(false);
            var childKey = sc.register(selector, SelectionKey.OP_READ);
            var childContext = new Context(childKey, (InetSocketAddress) sc.getRemoteAddress(), networkProcessor::processChildPacket, packetReader);
            childKey.attach(childContext);

            // Set application address once the client is connected to the remote server
            if(isRoot){
                networkProcessor.setApplicationAddress((InetSocketAddress) sc.getLocalAddress());
                networkProcessor.setState(NetworkState.CONNECTED); //TODO clean that
            }

            // Add the new child to the server client list
            networkProcessor.addChild(childContext);
        }catch (IOException e){
            logger.info("Client connection failed due to IOException !");
        }
    }

    /**
     * This method will only be call by the parent context when it was connected to the parent server
     * @throws IOException
     */
    private void doConnect(SelectionKey key) throws IOException {
        var sc = (SocketChannel) key.channel();
        if(!sc.finishConnect()){
            logger.warning("The selector lies");
            return;
        }

        // Secure check that application is not root
        if(isRoot){
            throw new AssertionError();
        }

        var previousAddress = networkProcessor.applicationAddress();

        // Set application address
        if(networkProcessor.getState() != NetworkState.CONNECTED){
            networkProcessor.setApplicationAddress((InetSocketAddress) sc.getLocalAddress());
            // Do not change the network state if it's currently under reconnection
            if(networkProcessor.getState() != NetworkState.RECONNECTION){
                networkProcessor.setState(NetworkState.CONNECTED);
            }
        }

        // Send connection packet
        key.interestOps(SelectionKey.OP_READ);

        // Send connection request to the parent
        if(networkProcessor.getState() == NetworkState.RECONNECTION){
            networkProcessor.manageReconnection(previousAddress);
        }else{
            networkProcessor.sendPacketToParent(new ConnectionPacket(networkProcessor.applicationAddress()));
        }
    }

    /**
     * Silently close the connection
     * @param key
     */
    private void silentlyClose(SelectionKey key) {
        var sc = (Channel) key.channel();
        try {
            sc.close();
            //clients.remove(key);
        } catch (IOException e) {
            // ignore exception
        }
    }
}
