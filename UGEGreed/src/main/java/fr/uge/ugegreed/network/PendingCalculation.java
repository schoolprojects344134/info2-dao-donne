package fr.uge.ugegreed.network;

import fr.uge.ugegreed.operation.CalculationRequestOperation;

import java.net.InetSocketAddress;

public record PendingCalculation(InetSocketAddress sender, CalculationRequestOperation calculationRequestOperation) {
}
