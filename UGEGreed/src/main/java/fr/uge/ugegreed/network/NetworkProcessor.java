package fr.uge.ugegreed.network;

import fr.uge.ugegreed.operation.*;
import fr.uge.ugegreed.operation.Operation;
import fr.uge.ugegreed.operation.CalculationRequestOperation;
import fr.uge.ugegreed.operation.LocalCalculationResultOperation;
import fr.uge.ugegreed.operation.NewCalculationOperation;
import fr.uge.ugegreed.packet.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class NetworkProcessor {
    private static final Logger logger = Logger.getLogger(NetworkProcessor.class.getName());
    private final NetworkManager networkManager;
    private final HashMap<InetSocketAddress, Context> routeTable = new HashMap<>();
    private final HashMap<InetSocketAddress, Integer> childrenWeight = new HashMap<>();
    private boolean acceptCalculation = true;
    private InetSocketAddress applicationAddress;
    private Context parentContext; // Parent context of the application where the application is connected
    private InetSocketAddress previousParentAddress; // Save previous parent on reconnection
    private boolean isRoot;
    private final ArrayList<Context> children = new ArrayList<>();
    private final ChildProcessor childProcessor = new ChildProcessor(this);
    private final ParentProcessor parentProcessor = new ParentProcessor(this);
    private final OperationProcessor operationProcessor = new OperationProcessor(this);
    private final HashMap<InetSocketAddress, Context> pendingConnections = new HashMap<>();
    private final ArrayDeque<PendingCalculation> pendingCalculationOperations = new ArrayDeque<>();
    private final Consumer<Operation> operationConsumer;
    private NetworkState state = NetworkState.LOCAL;
    private final HashSet<Long> calculationIds = new HashSet<>();
    private long calculationId = 0L;

    public NetworkProcessor(NetworkManager networkManager, Consumer<Operation> operationConsumer){
        this.networkManager = networkManager;
        this.operationConsumer = operationConsumer;
        this.isRoot = true;
    }

    public void setParent(Context parentContext){
        this.isRoot = false;
        this.parentContext = parentContext;
    }

    public void processOperation(Operation operation){
        logger.info((isRoot ? "ROOT" : "CLIENT") + " - " + applicationAddress + " - Received operation:  " + operation);
        switch (operation){
            case NewCalculationOperation op -> operationProcessor.processNewCalculationOperation(op);
            case CalculationRequestOperation op -> operationProcessor.processCalculationOperation(op);
            case DisconnectOperation op -> operationProcessor.processDisconnectOperation(op);
            case CalculationResultOperation op -> operationProcessor.processCalculationResultOperation(op);
            case UpdateApplicationCalculation op -> operationProcessor.processUpdateApplicationCalculation(op);
            case CalculationIdentifierRequestOperation op -> operationProcessor.processCalculationIdentifierRequestOperation(op);
            case CalculationIdentifierReleaseOperation op -> operationProcessor.processCalculationIdentifierReleaseOperation(op);
            case ShutdownResultOperation op ->  operationProcessor.processShutdownResultOperation(op);
            default -> throw new IllegalStateException("Unexpected value: " + operation);
        }
    }

    public NetworkState getState(){
        return state;
    }

    public void setState(NetworkState state){
        this.state = state;
    }

    /**
     * Send operation to the application
     * @param operation
     */
    public void sendOperation(Operation operation){
        operationConsumer.accept(operation);
    }

    /**
     * Process packet received from the parent
     * @param parentContext parent context
     * @param packet packet received
     */
    public void processParentPacket(Context parentContext, Packet packet) {
        logger.info("CLIENT " + " - " + applicationAddress + "- Received packet from parent: " + packet);
        switch (packet){
            case ConnectionPacket p -> logger.warning("Received ConnectionPacket that should not happen for parent: " + p);
            case ConnectionRefusedPacket p -> parentProcessor.processConnectionRefusedPacket(p, parentContext);
            case ConnectionAcceptPacket p -> parentProcessor.processConnectionAcceptPacket(p, parentContext);
            case RouteTableAddPathPacket p -> logger.warning("Received RouteTableAddPathPacket that should not happen for parent: " + p);
            case RouteTableRemovePathPacket p -> logger.warning("Received RouteTableRemovePathPacket that should not happen for parent: " + p);
            case IncrementWeightPacket p -> logger.warning("Received IncrementWeightPacket that should not happen for parent: " + p);
            case DecrementWeightPacket p -> logger.warning("Received DecrementWeightPacket that should not happen for parent: " + p);
            case NetworkWeightRequestPacket p -> logger.warning("Received NetworkWeightRequestPacket that should not happen for parent: " + p);
            case NetworkWeightResponsePacket p -> parentProcessor.processNetworkWeightResponsePacket(p, parentContext);
            case CalculationRequestPacket p -> parentProcessor.processCalculationRequestPacket(p, parentContext);
            case CalculationInterruptPacket p -> parentProcessor.processCalculationInterruptPacket(p, parentContext);
            case CalculationResultPacket p -> parentProcessor.processCalculationResultPacket(p, parentContext);
            case ReconnectionRequestPacket p -> parentProcessor.processReconnectionRequestPacket(p, parentContext);
            case ReconnectionPacket p -> logger.warning("Received ReconnectionPacket that should not happen for parent: " + p);
            case ReconnectionAcceptPacket p -> parentProcessor.processReconnectionAcceptPacket(p, parentContext);
            case CalculationIdentifierRequestPacket p -> logger.warning("Received CalculationIdentifierRequest that should not happen for parent: " + p);
            case CalculationIdentifierResultPacket p -> parentProcessor.processCalculationIdentifierResultPacket(p, parentContext);
            case CalculationIdentifierReleasePacket p -> logger.warning("Received CalculationIdentifierReleasePacket that should not happen for parent: " + p);
            default -> logger.warning("Unknown packet received: " + packet);
        }
    }

    /**
     * Process packet received from a child
     * @param childContext child sender context
     * @param packet packet received
     */
    public void processChildPacket(Context childContext, Packet packet){
        logger.info((isRoot ? "ROOT" : "CLIENT") + " - " + applicationAddress + " - Received packet from child: " + packet);
        switch (packet){
            case ConnectionPacket p -> childProcessor.processConnectionPacket(p, childContext);
            case ConnectionRefusedPacket p -> logger.warning("Received ConnectionRefusedPacket that should not happen for child");
            case ConnectionAcceptPacket p -> logger.warning("Received ConnectionAcceptPacket that should not happen for child");
            case RouteTableAddPathPacket p -> childProcessor.processRouteTableAddPathPacket(p, childContext);
            case RouteTableRemovePathPacket p -> childProcessor.processRouteTableRemovePathPacket(p, childContext);
            case IncrementWeightPacket p -> childProcessor.processIncrementWeightPacket(p, childContext);
            case DecrementWeightPacket p -> childProcessor.processDecrementWeightPacket(p, childContext);
            case NetworkWeightRequestPacket p -> childProcessor.processNetworkWeightRequestPacket(p, childContext);
            case NetworkWeightResponsePacket p -> logger.warning("Received NetworkWeightResponsePacket that should not happen for child");//childProcessor.processNetworkWeightResponsePacket(p, isRoot, childContext);
            case CalculationRequestPacket p -> childProcessor.processCalculationRequestPacket(p, childContext);
            case CalculationInterruptPacket p -> childProcessor.processCalculationInterruptPacket(p, childContext);
            case CalculationResultPacket p -> childProcessor.processCalculationResultPacket(p, childContext);
            case ReconnectionRequestPacket p -> logger.warning("Received ReconnectionRequestPacket that should not happen for parent: " + p);
            case ReconnectionPacket p -> childProcessor.processReconnectionPacket(p, childContext);
            case ReconnectionAcceptPacket p -> logger.warning("Received ReconnectionAcceptPacket that should not happen for parent: " + p);
            case CalculationIdentifierRequestPacket p -> childProcessor.processCalculationIdentifierRequest(p, childContext);
            case CalculationIdentifierResultPacket p -> logger.warning("Received CalculationIdentifierResult that should not happen for parent: " + p);
            case CalculationIdentifierReleasePacket p -> childProcessor.processCalculationIdentifierReleasePacket(p, childContext);
            default -> logger.warning("Unknown packet received: " + packet);
        }
    }

    /**
     * Check if application is a root application
     * @return return true if application is root false otherwise
     */
    public boolean isApplicationRoot(){
        return isRoot;
    }

    /**
     * Add route to the route table
     * @param destinationAddress address of the destination application
     * @param destinationContext context where the packet should be routed
     */
    public void addRoute(InetSocketAddress destinationAddress, Context destinationContext){
        routeTable.put(destinationAddress, destinationContext);
    }

    /**
     * Remove route from the route table
     * @param destinationAddress address to be removed
     */
    public void removeRoute(InetSocketAddress destinationAddress){
        routeTable.remove(destinationAddress);
    }

    /**
     * Get the child context whose ancestors contains the destination client.
     * If not found then the method return null.
     *
     * @param address address of the destination client
     * @return the child context where the packet should be routed or null if not found
     */
    public Context getRoute(InetSocketAddress address){
        return routeTable.get(address);
    }


    /**
     * Check is the application is the destination
     * @param destinationAddress destination address
     * @return true if application is the destination false otherwise
     */
    public boolean isApplicationReceiver(InetSocketAddress destinationAddress){
        return destinationAddress.equals(applicationAddress);
    }

    /**
     * Get the application address which represent the address of this own server
     * @return address of the application
     */
    public InetSocketAddress applicationAddress(){
        return applicationAddress;
    }

    public void setApplicationAddress(InetSocketAddress applicationAddress){
        this.applicationAddress = applicationAddress;
    }

    /**
     * Forward the packet to the parent of the application
     * @param packet packet to be forwarded
     */
    public void sendPacketToParent(Packet packet){
        if(isRoot){
            logger.warning("Root trying to send a packet to this parent: " + packet);
            return;
        }
        parentContext.queuePacket(packet);
    }

    /**
     * Increase by 1 the weight of the child
     *
     * @param child the child context
     */
    public void increaseWeight(Context child){
        childrenWeight.compute(child.getAddress(), (__, weight) -> weight == null ? 1 : weight + 1);
    }

    /**
     * Decrease by 1 the weight of the child
     * The weight can't be negative, the minimum is 0 and other operation will be ignored
     *
     * @param child the child context
     */
    public void decreaseWeight(Context child){
        childrenWeight.compute(child.getAddress(), (__, weight) -> (weight == null || weight == 0)  ? 0 : weight - 1);
    }

    /**
     * Return the total weight of all children in addition to the application weight
     *
     * @return total weight of the node (current application and all ancestors)
     */
    public long getNodeWeight(){
        return childrenWeight.values().stream().mapToInt(i -> i).sum() + (acceptCalculation ? 1 : 0);
    }

    /**
     * Add calculation to the calculation queue to wait for the network weight
     * @param sender sender address
     * @param calculationRequestOperation calculation operation request
     */
    public void queueCalculation(InetSocketAddress sender, CalculationRequestOperation calculationRequestOperation){
        pendingCalculationOperations.add(new PendingCalculation(sender, calculationRequestOperation));
    }

    /**
     * Save calculation requested by the client
     * @param localCalculationResultOperation calculation result operation
     */
    public void saveCalculationResult(LocalCalculationResultOperation localCalculationResultOperation){
        operationConsumer.accept(localCalculationResultOperation);
    }

    /**
     * Send to all child the reconnection request
     */
    public void requestChildrenReconnection(){
        for (var child: children){
            child.queuePacket(new ReconnectionRequestPacket(applicationAddress, parentContext.getAddress()));
        }
    }

    public void enableCalculation(){
        acceptCalculation = true;
    }

    public void disableCalculation(){
        acceptCalculation = false;
    }

    /**
     * Interrupt own calculation
     */
    public void interruptNetworkCalculation(){
        //TODO refactoring needed with interruptNetworkCalculationFromSender
        var packet = new CalculationInterruptPacket(applicationAddress);
        // Send packet to all children
        for (var child: children){
            child.queuePacket(packet);
        }
        // If not root forward packet to parent
        if(!isRoot){
            sendPacketToParent(packet);
        }
        sendOperation(new InterruptCalculationOperation(applicationAddress));
    }

    /**
     * Broadcast to all parent and children interruption packet only if it's not the sender
     * @param interruptedAddress address that should be interrupted
     * @param sender sender context
     */
    public void interruptNetworkCalculationFromSender(InetSocketAddress interruptedAddress, Context sender){
        var packet = new CalculationInterruptPacket(interruptedAddress);
        // Send packet to all children
        for (var child: children){
            if(sender.equals(child)){
                continue;
            }
            child.queuePacket(packet);
        }
        // If not root forward packet to parent
        if(!isRoot){
            // Send packet to parent only  if it's not the sender
            if(!parentContext.equals(sender)){
                sendPacketToParent(packet);
            }
        }
        // Interrupt calculation of the address
        sendOperation(new InterruptCalculationOperation(interruptedAddress));
    }

    public void addChild(Context childContext){
        children.add(childContext);
    }

    public void removeChild(Context childContext){
        children.remove(childContext);
    }

    /**
     * Process the next pending calculation and dispatch it over the network
     * @param networkWeight network weight
     */
    public void dispatchCalculation(long networkWeight){
        // Get calculation from pending calculation queue
        var pendingCalculation = pendingCalculationOperations.poll();
        if(pendingCalculation == null){
            return;
        }

        // Compute range of the calculation
        var calculationOperation = pendingCalculation.calculationRequestOperation();
        var calculationCount = calculationOperation.endRange() - calculationOperation.startRange();
        var applicationWeight = acceptCalculation ? 1 : 0;
        var totalChildrenWeight = childrenWeight.values().stream().mapToInt(i -> i).sum();
        var parentWeight = 0L;
        var totalWeight = 0L;
        var calculationSender = pendingCalculation.sender();

        // Check if the application is the sender of the calculation request
        totalWeight += applicationWeight;

        // Check if the sender is a child of the application
        var childSenderWeight = childrenWeight.get(calculationSender);
        // If sender is a child then remove the child weight from total weight otherwise simply add the total children weight
        if(childSenderWeight != null){
            totalWeight += totalChildrenWeight - childSenderWeight;
        }else{
            totalWeight += totalChildrenWeight;
        }

        // Compute parent weight if not root
        if(!isRoot){
            // Set parent weight
            parentWeight = networkWeight - totalWeight - ((childSenderWeight != null) ? childSenderWeight : 0);
            // Check if the application parent is not the sender
            if(!calculationSender.equals(parentContext.getAddress())){
                // Remove current node weight to the total network weight and add the result to the total weight of the node
                totalWeight += parentWeight;
            }
        }

        // If weight is lesser than 0 then
        if(totalWeight < 0){
            logger.warning("Negative network weight, can't process calculation dispatching.");
            return;
        }

        // If there is no remaining weight to do the calculation
        if(totalWeight == 0){
            //TODO need to wait before retrying processing calculation
            throw new AssertionError();
        }


        var calculationPart = 0L;
        // If the total weight is greater than calculation count (less than 1 calculation per client)
        if(totalWeight > calculationCount){
            calculationPart = 1;
        }else{
            // Set approximately the same range for the remote worker
            calculationPart = Math.ceilDiv(calculationCount, totalWeight); //TODO check that
        }

        // Dispatch calculation to children
        var startPosition = calculationOperation.startRange();
        var endPosition = calculationOperation.startRange();
        for(var child: children){
            // If child is the calculation sender then skip it
            if(child.getAddress().equals(calculationSender)){
                continue;
            }

            var childWeight = childrenWeight.get(child.getAddress());
            // Remove from total weight the weight of the child
            totalWeight -= childWeight;
            if(totalWeight <= 0){
                // Set range
                endPosition = calculationOperation.endRange();
            }else{
                endPosition += calculationPart * childWeight;
                if(endPosition > calculationOperation.endRange()){
                    endPosition = calculationOperation.endRange();
                }
            }

            var calculationPacket = new CalculationRequestPacket(calculationOperation.responseDestinationAddress(), calculationOperation.jarUrl(), calculationOperation.className(), startPosition, endPosition, calculationOperation.calculationId());
            startPosition = endPosition;
            child.queuePacket(calculationPacket);
            if(totalWeight <= 0 || startPosition == calculationOperation.endRange()){
                return;
            }
        }

        // Dispatch calculation to parent

        // Remove weight of the parent from total weight
        if(!isRoot && !calculationSender.equals(parentContext.getAddress())){
            totalWeight -= parentWeight;
            if(totalWeight <= 0){
                endPosition = calculationOperation.endRange();
            }else{
                endPosition += calculationPart * parentWeight;
                if(endPosition > calculationOperation.endRange()){
                    endPosition = calculationOperation.endRange();
                }
            }
            var calculationPacket = new CalculationRequestPacket(calculationOperation.responseDestinationAddress(), calculationOperation.jarUrl(), calculationOperation.className(), startPosition, endPosition, calculationOperation.calculationId());
            startPosition = endPosition;
            parentContext.queuePacket(calculationPacket);
            if(totalWeight <= 0 || startPosition == calculationOperation.endRange()){
                return;
            }
        }

        // Process owned application calculation part
        if(acceptCalculation){
            var localCalculationOperation = new CalculationRequestOperation(calculationOperation.responseDestinationAddress(), calculationOperation.jarUrl(), calculationOperation.className(), calculationOperation.calculationId(), startPosition, calculationOperation.endRange());
            sendOperation(localCalculationOperation);
        }

    }

    /**
     * Add a pending connection of a client
     *
     * @param newClientAddress the new client address who request a connection to the network
     * @param senderContext the sender context of the request
     */
    public boolean addPendingConnection(InetSocketAddress newClientAddress, Context senderContext){
        // If pending connection already contains the client address then refuse connection
        if(pendingConnections.containsKey(newClientAddress)){
            return false;
        }
        // Otherwise add the new pending connection
        pendingConnections.put(newClientAddress, senderContext);
        return true;
    }

    /**
     * Return context of the pending connection or null if there is no pending connection for this address
     *
     * @param newClientAddress new client that request connection to the network
     * @return  the context which know where is the requester client
     */
    public Context removePendingConnection(InetSocketAddress newClientAddress){
        return pendingConnections.remove(newClientAddress);
    }

    public int childCount(){
        return children.size();
    }

    public void disconnection(){
        networkManager.disconnect();
    }


    public void reconnection(InetSocketAddress destinationAddress) throws IOException {
        // Close parent connection
        previousParentAddress = parentContext.getAddress();
        parentContext.closeConnection();
        networkManager.connectionToParent(destinationAddress);
    }

    public void manageReconnection(InetSocketAddress previousApplicationAddress){
        if(previousParentAddress == null){
            throw new IllegalStateException();
        }
        var addresses = new ArrayList<>(children.stream().map(Context::getAddress).toList());
        addresses.add(previousApplicationAddress);
        sendPacketToParent(
                new ReconnectionPacket(
                        previousParentAddress,
                        addresses,
                    getNodeWeight()
                )
        );
        previousParentAddress = null;
    }

    public long generateCalculationId(){
        if(!isRoot){
            throw new AssertionError();
        }
        while (!calculationIds.add(calculationId)){
            calculationId++;
        }
        return calculationId;
    }

    public void releaseId(long calculationId){
        calculationIds.remove(calculationId);
    }
}
