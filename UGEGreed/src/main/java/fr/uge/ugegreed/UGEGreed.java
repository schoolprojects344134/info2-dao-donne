package fr.uge.ugegreed;

import fr.uge.ugegreed.client.CalculationManager;
import fr.uge.ugegreed.client.LocalCalculationRequest;
import fr.uge.ugegreed.network.NetworkManager;
import fr.uge.ugegreed.operation.*;
import fr.uge.ugegreed.operation.NewCalculationOperation;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.logging.Logger;

public class UGEGreed {
    private static Logger logger = Logger.getLogger(UGEGreed.class.getName());
    private final NetworkManager networkManager;
    private final CalculationManager calculationManager;

    private UGEGreed(int applicationPort, Path calculationRepositoryPath, InetSocketAddress serverAddress) throws IOException {
        this.networkManager = new NetworkManager(applicationPort, serverAddress, this::processOperation); //TODO set sourceAddress :s now giving null serveraddress when is root
        this.calculationManager = new CalculationManager(calculationRepositoryPath, this::sendOperation);
    }

    /**
     * Create a root application
     *
     * @param applicationPort port of the application server
     * @param calculationRepositoryPath repository to store calculation results
     *
     * @return the root application
     * @throws IOException throw when the server encounters a problem
     */
    public static UGEGreed createRootApplication(int applicationPort, Path calculationRepositoryPath) throws IOException {
        return new UGEGreed(applicationPort, calculationRepositoryPath, null);
    }

    /**
     * Create a client application
     *
     * @param applicationPort port of the application server
     * @param calculationRepositoryPath repository to store calculation results
     * @param serverAddress address of the server to which the client must connect
     *
     * @return the client application
     * @throws IOException throw when the server encounters a problem
     */
    public static UGEGreed createClientApplication(int applicationPort, Path calculationRepositoryPath, InetSocketAddress serverAddress) throws IOException {
        return new UGEGreed(applicationPort, calculationRepositoryPath, serverAddress);
    }


    /**
     * Wait for the user to process command
     */
    public void start(){
        // TODO Refactoring neeeeded
        commandUsage();
        try(var scanner = new Scanner(System.in)){
            while (scanner.hasNextLine()){
                try{
                    var next = scanner.nextLine();
                    var commandSplit = next.split(" ");
                    if(commandSplit.length == 6 && commandSplit[0].equals("START")){
                        createCalculation(commandSplit);
                    } else if (commandSplit.length == 1 && commandSplit[0].equals("DISCONNECT")) {
                        // Send a disconnection request
                        sendOperation(new DisconnectOperation());
                        break;
                    }else{
                        System.out.println("Unrecognized command !");
                        commandUsage();
                    }
                }catch (NumberFormatException e){
                    System.out.println("Error, invalid range given (not numbers).");
                }catch (IllegalArgumentException e){
                    System.out.println("Error, jar URL do not respect RFC 2396.");
                }catch (IOException e) {
                    System.out.println("Failed to create file for calculation results !");
                }
            }
        }
        logger.info("Application closing..., we hope you enjoyed using it ;D");
    }


    void createCalculation(String[] args) throws IOException {
        // Parse user command
        var jarURL = URI.create(args[1]);
        var className = args[2];
        int startRange = Integer.parseInt(args[3]);
        int endRange = Integer.parseInt(args[4]);
        var filename = args[5];
        // Check that calculation range is correct
        if(endRange <= startRange){
            System.out.println("Invalid calculation range : " + startRange + " -> " + endRange);
            return;
        }
        //TODO manage exception URI, Parse
        //Send a calculation request
        //var calculationId = calculationManager.createCalculation(filename, endRange - startRange);
        //sendOperation(new NewCalculationOperation(jarURL,className,calculationId, startRange, endRange));
        calculationManager.queueCalculation(new LocalCalculationRequest(filename, jarURL,className, startRange, endRange));
        sendOperation(new CalculationIdentifierRequestOperation());
    }

    /**
     * Send an operation to the network manager
     * @param operation operation requested
     */
    void sendOperation(Operation operation){
        networkManager.queueOperation(operation);
    }


    /**
     * Add calculation to the workers
     */
    private void processOperation(Operation operation){
        try {
            calculationManager.processOperation(operation);
        } catch (IOException e) {
            logger.severe("Fatal error when processing calculation operation: " + e.getCause());
        }
    }

    /**
     * Simple command usage print in console
     */
    private static void commandUsage(){
        System.out.println("""
                        Usage:
                        START url-jar fully-qualified-name start-range end-range filename
                        DISCONNECT
                       """);
    }

}
