package fr.uge.ugegreed;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {
    private static void usage() {
        System.out.println("Usage : UGEGreed port repositoryPath (Optional: serverAddress serverPort) ");
    }

    // TODO manage IOException inside the main ! test
    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to UGEGreed !");
        if (args.length == 2 || args.length == 4) {
            // Check calculation repository path
            var path = Path.of(args[1]);
            if(!Files.isDirectory(path)){
                System.out.println("No directory found for path: " + path);
                return;
            }
            try{
                // Check server port
                var serverPort = Integer.parseInt(args[0]);
                if(serverPort < 0 || serverPort > 65535){
                    System.out.println("Wrong server port was given, it should be in range of 0 to 65535");
                    return;
                }
                // Start application as client otherwise start it as root
                if (args.length == 4) {
                    try{
                        // Check parent port
                        var parentPort = Integer.parseInt(args[3]);
                        if(parentPort < 0 || parentPort > 65535){
                            System.out.println("Wrong parent port was given, it should be in range of 0 to 65535");
                            return;
                        }
                        try{
                            // Check parent address
                            var parentAddress = new InetSocketAddress(args[2], parentPort);
                            // Start client application
                            UGEGreed.createClientApplication(serverPort, path, parentAddress).start();
                        }catch (SecurityException e){
                            System.out.println("Failed to resolve the parent address, permission denied !");
                        }
                    }catch (NumberFormatException e){
                        System.out.println("Invalid parent port, it should be a number between 0 and 65535");
                    }
                }else{
                    // Start root application
                    UGEGreed.createRootApplication(serverPort, path).start();
                }
            }catch (NumberFormatException e){
                System.out.println("Invalid server port, it should be a number between 0 and 65535");
            }
        }else{
            usage();
        }
    }
}