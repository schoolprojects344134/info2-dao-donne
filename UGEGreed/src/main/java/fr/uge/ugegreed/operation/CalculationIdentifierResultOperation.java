package fr.uge.ugegreed.operation;

public record CalculationIdentifierResultOperation(long calculationIdentifier) implements Operation {
}
