package fr.uge.ugegreed.operation;

import java.net.InetSocketAddress;
import java.net.URI;

public record CalculationRequestOperation(InetSocketAddress responseDestinationAddress, URI jarUrl, String className, long calculationId, long startRange, long endRange) implements CalculableOperation {
}
