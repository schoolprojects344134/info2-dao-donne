package fr.uge.ugegreed.operation;

import java.net.URI;

public record NewCalculationOperation(URI jarUrl, String className, long calculationId, long startRange, long endRange) implements CalculableOperation {
}
