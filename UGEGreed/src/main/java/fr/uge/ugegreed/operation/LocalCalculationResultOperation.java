package fr.uge.ugegreed.operation;

import java.util.List;

public record LocalCalculationResultOperation(long calculationId, List<String> responses) implements CalculationResultOperation {
}
