package fr.uge.ugegreed.operation;

import java.util.List;

public record RemoteCalculationResultOperation(CalculableOperation calculableOperation, long calculationId, List<String> responses) implements CalculationResultOperation {
}
