package fr.uge.ugegreed.operation;

public record UpdateApplicationCalculation(boolean activated) implements Operation {
}
