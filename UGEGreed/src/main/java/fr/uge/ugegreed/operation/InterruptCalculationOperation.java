package fr.uge.ugegreed.operation;

import java.net.InetSocketAddress;

public record InterruptCalculationOperation(InetSocketAddress ownerAddress) implements Operation {
}
