package fr.uge.ugegreed.operation;

public record ShutdownResultOperation() implements Operation {
}
