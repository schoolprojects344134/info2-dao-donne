package fr.uge.ugegreed.operation;

import java.net.InetSocketAddress;

public record DispatchCalculationOperation(InetSocketAddress applicationAddress) implements Operation {
}
