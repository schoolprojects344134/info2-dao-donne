package fr.uge.ugegreed.operation;

import java.net.URI;
import java.net.URL;

public interface CalculableOperation extends Operation{
    long startRange();
    long endRange();
    URI jarUrl();
    String className();
    long calculationId();
}
