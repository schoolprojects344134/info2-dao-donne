package fr.uge.ugegreed.operation;

public record CalculationIdentifierReleaseOperation(long calculationId) implements Operation {
}
