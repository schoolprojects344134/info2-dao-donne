package fr.uge.ugegreed.client;

import fr.uge.ugegreed.operation.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.function.Consumer;

public class CalculationManager {
    private final CalculationWorker calculationWorker;
    private final ClientCalculation clientCalculation;
    private final Deque<LocalCalculationRequest> newCalculationQueue = new ArrayDeque<>();
    private final Consumer<Operation> operationSender;

    public CalculationManager(Path calculationRepositoryPath, Consumer<Operation> operationSender){
        this.calculationWorker = new CalculationWorker(operationSender);
        clientCalculation = new ClientCalculation(calculationRepositoryPath);
        this.operationSender = operationSender;
    }

    public void queueCalculation(LocalCalculationRequest localCalculationRequest){
        newCalculationQueue.add(localCalculationRequest);
    }

    /**
     * Create a new calculation operation
     * @param calculationIdentifier calculation id
     * @return the new calculation operation
     * @throws IOException if there is an error while creating the file which store calculation results
     */
    public NewCalculationOperation createCalculation(long calculationIdentifier) throws IOException {
        var nextCalculation = newCalculationQueue.poll();
        if(nextCalculation == null){
            throw new IllegalStateException();
        }
        clientCalculation.createCalculation(nextCalculation.fileName(), calculationIdentifier, nextCalculation.endRange() - nextCalculation.startRange());
        return new NewCalculationOperation(nextCalculation.jarUrl(),nextCalculation.className(),calculationIdentifier, nextCalculation.startRange(), nextCalculation.endRange());
    }


    /**
     * Add calculation request the workers processing queue
     * @param calculableOperation calculation operation submitted
     */
    private void processCalculationRequest(CalculableOperation calculableOperation){
        calculationWorker.addCalculation(calculableOperation);
    }

    /**
     * Add calculation results to the client owned calculations
     * @param calculationId identifier of the calculation
     * @param calculationResults calculation resutls list
     * @throws IOException
     */
    private void addCalculationResults(long calculationId, List<String> calculationResults) throws IOException {
        clientCalculation.addResult(calculationId, calculationResults, operationSender);
    }

    /**
     * Process operation
     * @param operation operation to be processed
     * @throws IOException
     */
    public void processOperation(Operation operation) throws IOException {
        switch (operation){
            case CalculableOperation op -> processCalculationRequest(op);
            case LocalCalculationResultOperation op -> addCalculationResults(op.calculationId(), op.responses());
            case CalculationIdentifierResultOperation op -> operationSender.accept(createCalculation(op.calculationIdentifier()));
            case InterruptCalculationOperation op -> calculationWorker.interruptCalculation(op.ownerAddress());
            case DispatchCalculationOperation op -> calculationWorker.dispatchCalculation(op.applicationAddress());
            default -> throw new AssertionError("Unexpected value: " + operation);
        }
    }

}
