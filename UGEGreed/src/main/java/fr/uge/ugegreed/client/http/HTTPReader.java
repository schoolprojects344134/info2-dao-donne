package fr.uge.ugegreed.client.http;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.http.ChunkReader;
import fr.uge.ugegreed.packet.reader.http.HeaderReader;

public class HTTPReader implements Reader<Path>{
  private ReaderState state = ReaderState.WAITING;
  private ChunkReader chunkReader = new ChunkReader();
  private HeaderReader headerReader = new HeaderReader();
  private HTTPHeader header;
  private ByteBuffer readBuffer;
  private static Logger logger = Logger.getLogger(HTTPReader.class.getName());
  
  @Override
  public ProcessStatus process(ByteBuffer bb) {
    if (state == ReaderState.DONE || state == ReaderState.ERROR) {
      throw new IllegalStateException();
    }
    if(header == null){
      var headerProcess = headerReader.process(bb);
      if (headerProcess != ProcessStatus.DONE) {
        if (headerProcess == ProcessStatus.ERROR) {
          state = ReaderState.ERROR;
        }
        return headerProcess;
      }
      header = headerReader.get();

      if(!header.getContentType().isPresent() ||
          !"application/java-archive".equals(header.getContentType().get())) {
        logger.warning("Ressource is not a java-archive");
        state = ReaderState.ERROR;
        return ProcessStatus.ERROR;
      }
    }

    try {
      if(header.getContentLength() != -1) {
        logger.info("headerContent");
        var contentLength = header.getContentLength();
        readBuffer = ByteBuffer.allocate(contentLength);
        bb.flip(); // mode lecture
        try {
          if (bb.remaining() <= contentLength) {
            logger.info("avant put readerBuffer");
            readBuffer.put(bb);
          } else {
            var oldLimit = bb.limit();
            bb.limit(contentLength);
            readBuffer.put(bb);
            bb.limit(oldLimit);
          }
        } finally {
          bb.compact();// mode ecriture
          logger.info("compact bb");
        }

        if (readBuffer.position() < contentLength) {
          return ProcessStatus.REFILL;
        }
      }
    } catch (HTTPException e) {
      logger.severe("Unexpecting error when reading Header");
      state = ReaderState.ERROR;
      return ProcessStatus.ERROR;
    }
    
    if(header.getTransferEncoding().isPresent()) {
      logger.info("chunk");
      var chunkProcess = chunkReader.process(bb);
      if (chunkProcess != ProcessStatus.DONE) {
        if (chunkProcess == ProcessStatus.ERROR) {
          state = ReaderState.ERROR;
        }
        return chunkProcess;
      }
      readBuffer = chunkReader.get();
    }
    logger.info("done ?");
    state = ReaderState.DONE;
    return ProcessStatus.DONE;
  }

  @Override
  public Path get() {
    if (state != ReaderState.DONE) {
      throw new IllegalStateException();
    }
    try {
      var path = Files.createTempFile("UGEGreed_", "");
      Files.write(path, readBuffer.flip().array());
      return path;
    } catch (IOException e) {
      logger.log(Level.SEVERE ,"Bug during the creation of the file", e);
      throw new IllegalStateException();
    }
    
  }

  @Override
  public void reset() {
    state = ReaderState.WAITING;
    header = null;
    headerReader.reset();
    readBuffer = null;
    chunkReader.reset();
  }

}
