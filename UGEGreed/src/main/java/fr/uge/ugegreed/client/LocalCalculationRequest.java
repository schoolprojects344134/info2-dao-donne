package fr.uge.ugegreed.client;

import fr.uge.ugegreed.operation.NewCalculationOperation;

import java.net.URI;

public record LocalCalculationRequest(String fileName, URI jarUrl, String className, long startRange, long endRange) {
}
