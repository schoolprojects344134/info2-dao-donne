package fr.uge.ugegreed.client;

import fr.uge.ugegreed.Checker;
import fr.uge.ugegreed.client.http.HTTPClient;
import fr.uge.ugegreed.network.NetworkManager;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

public class CacheManager {
  private static final Logger logger = Logger.getLogger(CacheManager.class.getName());

  private HTTPClient httpClient;

  private final URL url;

  private final String className;

  private final boolean local;

  /**
   * Public constructor to call a resource from the internet
   *
   * @param url       url to the ressource
   * @param classname name of the resource
   */
  public CacheManager(URL url, String classname) {
      Objects.requireNonNull(url, "URL cannot be null");
      Objects.requireNonNull(classname, "ClassName cannot be null");

      this.url = url;
      this.className = classname;
      this.local = false;
      this.downloadJar();
  }

  private void downloadJar(){
    try {
      this.httpClient = new HTTPClient(url);
      httpClient.launch();
      httpClient.queueOwnCommand();
      // TODO getResult
    } catch (IOException e) {
      throw new IllegalStateException("Address unable to connect to");
    }

    // TODO être sûr de récupérer le jar
  }

  /**
   * Public constructor to call a resource from the local disk
   *
   * @param path      path to the ressource
   * @param classname name of the ressource
   */
  public CacheManager(Path path, String classname) {
    try {
      Objects.requireNonNull(path, "Path cannot be null");
      Objects.requireNonNull(classname, "Classname cannot be null");

      this.url = path.toUri().toURL();
      this.className = classname;
      this.local = true;
    } catch (MalformedURLException e) {
      logger.severe("URL is malformed");
      throw new IllegalArgumentException("URL is malformed");
    }
  }


  /**
   * Retrieve the checker from the resource of this cache
   *
   * @return The checker for the jar at the given url when creating the object
   */
  public Optional<Checker> retrieve() {
    var urls = new URL[]{url};
    var urlClassLoader = new URLClassLoader(urls, Thread.currentThread().getContextClassLoader());
    try {
      var clazz = Class.forName(className, true, urlClassLoader);
      var constructor = clazz.getDeclaredConstructor();
      var instance = constructor.newInstance();
      return Optional.of((Checker) instance);
    } catch (ClassNotFoundException e) {
      logger.info("The class %s was not found in %s. The jarfile might not be present at the given URL.".formatted(className, url));
      return Optional.empty();
    } catch (NoSuchMethodException e) {
      logger.info("Class %s in jar %s cannot be cast to fr.uge.ugegreed.Checker".formatted(className, url));
      return Optional.empty();
    } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
      logger.info("Failed to create an instance of %s".formatted(className));
      return Optional.empty();
    }
  }
}
