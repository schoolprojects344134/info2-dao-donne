package fr.uge.ugegreed.client;

import fr.uge.ugegreed.network.NetworkProcessor;
import fr.uge.ugegreed.operation.*;

import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class CalculationWorker {
    private static final Logger logger = Logger.getLogger(CalculationWorker.class.getName());
    private final Object lock = new Object();
    private static final int MAXIMUM_CALCULATION_CAPACITY = 1_000_000;
    private static final int MAXIMUM_WORKER_COUNT = 20;
    private final ExecutorService executor = Executors.newFixedThreadPool(MAXIMUM_WORKER_COUNT);
    private final ArrayBlockingQueue<Calculation> calculationsQueue = new ArrayBlockingQueue<>(MAXIMUM_CALCULATION_CAPACITY); // Processing queue of calculation
    private final ArrayDeque<Calculation> pendingCalculation = new ArrayDeque<>(); // Store pending calculation when the processing queue is full
    private final ArrayList<Calculation> workingCalculations = new ArrayList<>(); // Contains calculation under calculation
    private final Consumer<Operation> calculationOperationSender;
    private boolean shutdown;

    public CalculationWorker(Consumer<Operation> calculationOperationSender){
        this.calculationOperationSender = calculationOperationSender;
        // Start calculation processing thread
        Thread.ofPlatform().start(() -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    var nextCalculation = calculationsQueue.take();
                    synchronized (lock){
                        if(pendingCalculation.size() > 0){
                            calculationsQueue.put(pendingCalculation.remove());
                        }
                        workingCalculations.add(nextCalculation);
                    }
                    // Create future calculation
                    var future = executor.submit(() -> {
                        try {
                            // While there are remaining calculation
                            while (nextCalculation.hasNext()){
                                // Process next calculation
                                nextCalculation.processNext();
                            }
                        } catch (InterruptedException e) {
                            // Check if calculation need to be dispatched
                            if(nextCalculation.needDispatch() && nextCalculation.hasNext()){
                                // Send new calculation request if there is remaining calculation but skip interrupted calculation
                                if(!nextCalculation.isInterrupted()){
                                    var r = nextCalculation.dispatchCalculation();
                                    logger.info("DISPATCHING CALCULATION " + r);
                                    calculationOperationSender.accept(r);
                                }else{
                                    var r = nextCalculation.dispatchCalculation();
                                    logger.info("INTERRUPT CALCULATION " + r);
                                }
                            }
                        }
                        // Return calculation result
                        synchronized (lock){
                            workingCalculations.remove(nextCalculation);
                        }
                        // Return calculation result complete or part of the calculation depending on dispatch and interrupt
                        return nextCalculation.getResult();
                    });
                    // Send calculation result
                    this.calculationOperationSender.accept(future.get());
                    // Check if finished
                    synchronized (lock){
                        // If worker finished all his work then close the connection
                        if(shutdown && workingCalculations.isEmpty()){
                            this.calculationOperationSender.accept(new ShutdownResultOperation());
                            return;
                        }
                    }
                }
            } catch (InterruptedException e) {
                // managed in finally
            } catch (ExecutionException e) {
                logger.severe("Error while processing calculations: " + e);
            }finally {
                logger.info("Closing calculation workers");
            }
        });
    }


    public void addCalculation(CalculableOperation calculableOperation) {
        try {
            // Retrieve checker
            var checker = ClientJar.retrieveCheckerFromURL(calculableOperation.jarUrl().toURL(), calculableOperation.className()).orElseThrow(); //TODO replace URI per URL
            var calculation = new Calculation(calculableOperation, checker);
            // If working queue is full
            synchronized (lock){
                if(calculationsQueue.size() == MAXIMUM_CALCULATION_CAPACITY){
                    pendingCalculation.add(calculation); // Add to pending calculation
                }else{
                    calculationsQueue.add(calculation); // Add to calculation processing queue
                }
            }
        } catch (MalformedURLException e) {
            // Should not happen if checked at the URL creation
        }
    }

    public void interruptCalculation(InetSocketAddress ownerAddress){
        // Interrupt processing queue calculations
        calculationsQueue.forEach(calculation -> calculation.interrupt(ownerAddress));
        // Interrupt working and pending calculations
        synchronized (lock){
            workingCalculations.forEach(calculation -> calculation.interrupt(ownerAddress));
            pendingCalculation.forEach(calculation -> calculation.interrupt(ownerAddress));
        }
    }

    public void dispatchCalculation(InetSocketAddress applicationAddress){
        // Dispatch calculations from calculation processing queue and clear queue
        calculationsQueue.forEach(calculation -> {
            // Send dispatch request
            calculation.dispatchRequest(applicationAddress);
            // Only dispatch calculation that are not interrupted
            if(!calculation.isInterrupted()){
                calculationOperationSender.accept(calculation.dispatchCalculation());
            }
        });
        calculationsQueue.clear();
        synchronized (lock){
            // Dispatch calculations currently processed but do not clear the list to let them shutdown properly
            workingCalculations.forEach(calculation -> calculation.dispatchRequest(applicationAddress));
            // Dispatch and clear pending calculations
            pendingCalculation.forEach(calculation -> {
                // Send dispatch request
                calculation.dispatchRequest(applicationAddress);
                // Only dispatch calculation that are not interrupted
                if(!calculation.isInterrupted()){
                    calculationOperationSender.accept(calculation.dispatchCalculation());
                }
            });
            pendingCalculation.clear();
            shutdown = true;
        }
    }


}
