package fr.uge.ugegreed.client;

import fr.uge.ugegreed.Checker;
import fr.uge.ugegreed.operation.*;

import java.net.InetSocketAddress;
import java.util.ArrayList;

public class Calculation{
    private final Object lock = new Object();
    private final Checker checker;
    private final CalculableOperation calculableOperation;
    private final long startRange;
    private final long endRange;
    private long position;
    private final ArrayList<String> calculationResults = new ArrayList<>();
    private boolean interrupted;
    private boolean needDispatch;


    public Calculation(CalculableOperation calculableOperation, Checker checker){
        this.calculableOperation = calculableOperation;
        this.startRange = calculableOperation.startRange();
        this.endRange = calculableOperation.endRange();
        this.checker = checker;
        this.position = startRange;
    }

    /**
     * Check if there is remaining calculation
     * @return true if there is calculation remaining false otherwise
     */
    public boolean hasNext(){
        return position < endRange;
    }

    /**
     * Process next calculation (Thread safe)
     *
     * @throws InterruptedException throw if thread is interrupted while processing calculation
     */
    public void processNext() throws InterruptedException {
        synchronized (lock){
            if(interrupted || needDispatch){
                Thread.currentThread().interrupt();
                return;
            }
        }
        if(!hasNext()){
            throw new IllegalStateException();
        }
        calculationResults.add(checker.check(position));
        position++;
    }

    /**
     * Get calculation results operation (Thread safe)
     * @return the calculation result operation
     */
    public CalculationResultOperation getResult(){
        synchronized (lock){
            // Get result when there is remaining calculation only when dispatching and interrupting
            if(hasNext() && !interrupted && !needDispatch){
                throw  new IllegalStateException();
            }
            return switch (calculableOperation){
                case CalculationRequestOperation op -> new RemoteCalculationResultOperation(op, op.calculationId(), calculationResults);
                case NewCalculationOperation op -> new LocalCalculationResultOperation(op.calculationId(), calculationResults);
                default -> throw new IllegalStateException("Unexpected value: " + calculableOperation);
            };
        }
    }

    /**
     * Interrupt the calculation if destination address are the same
     * @param otherAddress address of application which calculations should be interrupted
     */
    public void interrupt(InetSocketAddress otherAddress){
        switch (calculableOperation){
            case CalculationRequestOperation op -> {
                if(op.responseDestinationAddress().equals(otherAddress)){
                    synchronized (lock){
                        interrupted = true;
                    }
                }
            }
            case NewCalculationOperation op -> {
                //Do nothing here, it's a local calculation, so it will be interrupted when application disconnect automatically
            }
            default -> throw new IllegalStateException("Unexpected value: " + calculableOperation);
        }
    }

    public boolean isInterrupted(){
        synchronized (lock){
           return interrupted;
        }
    }

    public boolean needDispatch(){
        synchronized (lock){
            return needDispatch;
        }
    }

    public void dispatchRequest(InetSocketAddress otherAddress){
        synchronized (lock){
            needDispatch = true;
            // Interrupt if it's owned calculation
            interrupt(otherAddress);
        }
    }

    public CalculableOperation dispatchCalculation(){
        // Check that calculation is not interrupted and need dispatch
        synchronized (lock){
            if(!needDispatch || interrupted){
                throw new IllegalStateException();
            }
        }
        return switch (calculableOperation){
            case CalculationRequestOperation op -> new CalculationRequestOperation(op.responseDestinationAddress(), op.jarUrl(), op.className(), op.calculationId(), position, op.endRange());
            case NewCalculationOperation op ->  throw new AssertionError();//new NewCalculationOperation(op.jarUrl(), op.className(), op.calculationId(), position, op.endRange()); // TODO check if it may be removed
            default -> throw new IllegalStateException("Unexpected value: " + calculableOperation);
        };
    }


}
