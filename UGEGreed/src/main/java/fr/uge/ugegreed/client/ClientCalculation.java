package fr.uge.ugegreed.client;

import fr.uge.ugegreed.network.NetworkManager;
import fr.uge.ugegreed.operation.CalculationIdentifierReleaseOperation;
import fr.uge.ugegreed.operation.Operation;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static java.nio.file.StandardOpenOption.*;

public class ClientCalculation {
    private static final Logger logger = Logger.getLogger(ClientCalculation.class.getName());
    private final Path calculationRepositoryPath;
    private final HashMap<Long, CalculationResult> clientCalculations = new HashMap<>();

    /**
     * Create a new client calculation which manage client's calculation
     * @param calculationRepositoryPath the directory path where calculation result should be saved
     */
    public ClientCalculation(Path calculationRepositoryPath) {
        this.calculationRepositoryPath = calculationRepositoryPath;
    }

    /**
     * Create a new calculation which create a result receiver and an id for the calculation
     *
     * @param fileName output filename where the results should be stored
     * @param expectedResultCount expected calculation count (total range of calculation)
     * @throws IOException throw this exception if there is a wrong path to the file
     */
    public void createCalculation(String fileName, long calculationId, long expectedResultCount) throws IOException {
        // Create file to check if path is correct
        var filePath = Path.of(calculationRepositoryPath.toString(), fileName);
        if(!Files.exists(filePath)){
            Files.createFile(filePath);
        }
        var calculationResult = new CalculationResult(filePath, expectedResultCount);
        clientCalculations.put(calculationId,calculationResult);
    }

    /**
     * Add calculation result
     * @param calculationId calculation identifier
     * @param calculationResults calculation results
     * @throws IOException thrown if there is a problem while saving results
     */
    public void addResult(long calculationId, List<String> calculationResults, Consumer<Operation> operationSender) throws IOException {
        // Get the calculation receiver
        var calculation = clientCalculations.get(calculationId);
        // Can happen in some rare cases, it's not a bug (more a rat on the network)
        if(calculation == null){
            return;
        }
        // Add results to the calculation receiver
        if(calculation.addResults(calculationResults)){
            // If calculation is done then remove it from map
            clientCalculations.remove(calculationId);
            // Send operation to release calculation id
            operationSender.accept(new CalculationIdentifierReleaseOperation(calculationId));
        }
    }
}
