package fr.uge.ugegreed.client;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.*;

public class CalculationResult {
    private final Path filePath;
    private final long expectedResultCount;
    private final ArrayList<String> calculationResults = new ArrayList<>();
    private long processedResult;
    public CalculationResult(Path filePath, long expectedResultCount){
        this.filePath = filePath;
        this.expectedResultCount = expectedResultCount;
    }

    /**
     * Add result to the client calculation
     * @param result calculation result
     * @return true if calculation is finished and false otherwise
     * @throws IOException thrown if there is an error while saving results in file
     */
    public boolean addResult(String result) throws IOException {
        if(processedResult == expectedResultCount){
            throw new IllegalStateException("Result already finished" + result);
        }
        calculationResults.add(result);
        processedResult++;
        // If finished or if list is full (size of list = Integer.MAX_VALUE)
        if(processedResult == expectedResultCount || calculationResults.size() == Integer.MAX_VALUE){
            saveResults();
        }
        // Return true if all calculation result were received
        return processedResult == expectedResultCount;
    }

    public boolean addResults(List<String> results) throws IOException {
        for(var result : results){
            addResult(result);
        }
        // Return true if all calculation result were received
        return processedResult == expectedResultCount;
    }

    /**
     * Save results in calculation result file
     * @throws IOException
     */
    private void saveResults() throws IOException {
        Files.write(filePath, calculationResults, StandardCharsets.UTF_8, CREATE, WRITE, TRUNCATE_EXISTING);
        calculationResults.clear();
    }
}
