package fr.uge.ugegreed.client.http;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HTTPClient {

  static private class Context {
    private SelectionKey key;
    private final SocketChannel sc;
    private final ByteBuffer bufferIn = ByteBuffer.allocate(BUFFER_SIZE);
    private final ByteBuffer bufferOut = ByteBuffer.allocate(BUFFER_SIZE);
    private final ArrayDeque<ByteBuffer> dequeRequest = new ArrayDeque<>();
    private Boolean closed = false;
    private final Charset ASCII = StandardCharsets.US_ASCII;
    private final HTTPReader httpReader = new HTTPReader();
    private final ArrayBlockingQueue<Path> responseQueue = new ArrayBlockingQueue<>(1);

    private Context(SelectionKey key) {
      this.key = key;
      this.sc = (SocketChannel) key.channel();
    }

    public void doRead() throws IOException {
      if (sc.read(bufferIn) == -1) {
        closed = true;
      }
      processIn();
      updateInterestOps();
    }

    public void doWrite() throws IOException {    
      bufferOut.flip();
      sc.write(bufferOut);
      bufferOut.compact();
      processOut();
      updateInterestOps();
    }

    public Path getResult() throws InterruptedException {
      return responseQueue.take();
    }

    private void processIn() {
      for (;;) {
        var status = httpReader.process(bufferIn);
        switch (status) {
        case DONE:
          var header = httpReader.get();
          System.out.println(header);
          httpReader.reset();
          responseQueue.add(header);
          break;
        case REFILL:
          return;
        case ERROR:
          silentlyClose();
          return;
        }
      }
    }

    private void processOut() {
      if (dequeRequest.isEmpty() || bufferOut.remaining() == 0) {
        return;
      }
      var nextRequest = dequeRequest.peekFirst();
      if (nextRequest.remaining() <= bufferOut.remaining()) {
        bufferOut.put(nextRequest);
        dequeRequest.removeFirst();
        return;
      }
      var oldLimit = nextRequest.limit();
      nextRequest.limit(bufferOut.remaining());
      bufferOut.put(nextRequest);
      nextRequest.limit(oldLimit);
    }

    private void updateInterestOps() {
      var newInterestOps = 0;
      if (!closed && bufferIn.hasRemaining()) {
        newInterestOps |= SelectionKey.OP_READ;
      }
      if (bufferOut.position() != 0) {
        newInterestOps = SelectionKey.OP_WRITE;
      }
      if (newInterestOps == 0) {
        silentlyClose();
        return;
      }
      key.interestOps(newInterestOps);
    }

    private void silentlyClose() {
      try {
        sc.close();
      } catch (IOException e) {
        // ignore exception
      }
    }

    /**
     * 
     * @param key
     * @throws IOException
     */
    public void doConnect(SelectionKey key) throws IOException {
      if (!sc.finishConnect()) {
        return; // the selector gave a bad hint
      }
      key.interestOps(SelectionKey.OP_READ);
    }

    public void queueRequest(URL url) {
      var request = "GET " + url.getPath() + " HTTP/1.1\r\n" + "Host: " + url.getHost() + "\r\n\r\n";
      var encodeRequest = ASCII.encode(request);
      dequeRequest.add(encodeRequest);
      processOut();
      updateInterestOps();
    }

  }

  private static int BUFFER_SIZE = 10_000;
  private static Logger logger = Logger.getLogger(HTTPClient.class.getName());

  private final SocketChannel sc;
  private final Selector selector;
  private final InetSocketAddress serverAddress;
  private final String ressource;

  private final URL url;
  private Context uniqueContext;
  private final ArrayDeque<URL> dequeUrl = new ArrayDeque<>();
  private final Charset ASCII = StandardCharsets.US_ASCII;
  private final Object lock = new Object();

  public HTTPClient(URL url) throws IOException {
    this.url = url;
    this.serverAddress = new InetSocketAddress(url.getHost(), 80);
    this.ressource = url.getPath();
    this.sc = SocketChannel.open();
    this.selector = Selector.open();
  }

  public void launch() throws IOException {
    sc.configureBlocking(false);
    var key = sc.register(selector, SelectionKey.OP_CONNECT);
    uniqueContext = new Context(key);
    key.attach(uniqueContext);
    sc.connect(serverAddress);
    
    Thread.ofPlatform().start(() -> {
      while (!Thread.interrupted()) {
        try {
          selector.select(this::treatKey);
          processCommands();
        } catch (IOException tunneled) {
          logger.log(Level.SEVERE, "Connection terminated with client by IOException", tunneled.getCause());
          return;
        }
      }
    });
  }

  private void processCommands() {
    synchronized (lock) {
      var nextCommands = dequeUrl.poll();
      if(nextCommands == null) {
        return;
      }
      uniqueContext.queueRequest(nextCommands);
    }
  }

  public void queueOwnCommand(){
    queueCommand(url);
  }
  
  public void queueCommand(URL url) {
    synchronized (lock) {
      dequeUrl.add(url);
      selector.wakeup();
    }
  }

  private void treatKey(SelectionKey key) {
    logger.info("treatKey");
    try {
      if (key.isValid() && key.isConnectable()) {
        logger.info("doConnect");
        uniqueContext.doConnect(key);
      }
      if (key.isValid() && key.isWritable()) {
        logger.info("doWrite");
        uniqueContext.doWrite();
      }
      if (key.isValid() && key.isReadable()) {
        logger.info("doRead");
        uniqueContext.doRead();
      }
    } catch (IOException ioe) {
      // lambda call in select requires to tunnel IOException
      throw new UncheckedIOException(ioe);
    }
  }

  private void silentlyClose(SelectionKey key) {
    Channel sc = (Channel) key.channel();
    try {
      sc.close();
    } catch (IOException e) {
      // ignore exception
    }
  }

}
