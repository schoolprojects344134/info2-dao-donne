package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.writer.Writer;

import java.util.HashMap;
import java.util.Map;

public interface Packet {

    Writer createWriter();

    static Map<Integer,Reader<Packet>> createReaders(){
        var map = new HashMap<Integer,Reader<Packet>>();
        ConnectionPacket.createPacketReader(map);
        ConnectionRefusedPacket.createPacketReader(map);
        ConnectionAcceptPacket.createPacketReader(map);
        RouteTableAddPathPacket.createPacketReader(map);
        RouteTableRemovePathPacket.createPacketReader(map);
        IncrementWeightPacket.createPacketReader(map);
        DecrementWeightPacket.createPacketReader(map);
        NetworkWeightRequestPacket.createPacketReader(map);
        NetworkWeightResponsePacket.createPacketReader(map);
        CalculationRequestPacket.createPacketReader(map);
        CalculationInterruptPacket.createPacketReader(map);
        CalculationResultPacket.createPacketReader(map);
        ReconnectionRequestPacket.createPacketReader(map);
        ReconnectionPacket.createPacketReader(map);
        ReconnectionAcceptPacket.createPacketReader(map);
        DecrementWeightPacket.createPacketReader(map);
        DecrementWeightPacket.createPacketReader(map);
        DecrementWeightPacket.createPacketReader(map);
        CalculationIdentifierRequestPacket.createPacketReader(map);
        CalculationIdentifierResultPacket.createPacketReader(map);
        CalculationIdentifierReleasePacket.createPacketReader(map);
        return map;
    }
}
