package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.generic.IntReader;
import java.nio.ByteBuffer;
import java.util.Map;

public class PacketReader implements Reader<Packet>{
    private ReaderState state = ReaderState.WAITING;
    private final IntReader idReader = new IntReader();
    private int id = -1;
    private Reader<Packet> usedReader;
    private Packet resultPacket;
    private final Map<Integer, Reader<Packet>> readers = Packet.createReaders();

    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == ReaderState.DONE || state == ReaderState.ERROR) {
            throw new IllegalStateException();
        }

        // Reading packet id
        if(id < 0){
            var idProcessState = idReader.process(buffer);
            if(idProcessState != Reader.ProcessStatus.DONE){
                return idProcessState;
            }
            id = idReader.get();
            usedReader = readers.get(id);
            if(usedReader == null){
                state = ReaderState.ERROR;
                return ProcessStatus.ERROR;
            }
        }

        // Process selected reader
        var readerProcessState = usedReader.process(buffer);
        if(readerProcessState != ProcessStatus.DONE){
            return readerProcessState;
        }
        resultPacket = usedReader.get();
        state = ReaderState.DONE;
        return ProcessStatus.DONE;
    }

    @Override
    public Packet get() {
        if (state != ReaderState.DONE) {
            throw new IllegalStateException();
        }
        return resultPacket;
    }

    @Override
    public void reset() {
        state = ReaderState.WAITING;
        idReader.reset();
        usedReader.reset();
        usedReader = null;
        resultPacket = null;
        id = -1;
    }
}
