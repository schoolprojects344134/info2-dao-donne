package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.generic.AddressReader;
import fr.uge.ugegreed.packet.reader.generic.LongReader;
import fr.uge.ugegreed.packet.reader.generic.TextReader;
import fr.uge.ugegreed.packet.reader.generic.UTF8Reader;
import fr.uge.ugegreed.packet.writer.AddressWriter;
import fr.uge.ugegreed.packet.writer.TextWriter;
import fr.uge.ugegreed.packet.writer.Writer;
import fr.uge.ugegreed.packet.writer.WriterState;

import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Map;

// TODO check if better use URL instead of URI ?
public record CalculationRequestPacket(InetSocketAddress address, URI jarUrl, String className, long startRange, long endRange, long calculationId) implements Packet {
    private static final int ID = 10;
    static void createPacketReader(Map<Integer, Reader<Packet>> readers){
        var reader = new Reader<Packet>() {
            private ReaderState state = ReaderState.WAITING;
            private final AddressReader addressReader = new AddressReader();
            private final TextReader stringReader = new TextReader();
            private final LongReader longReader = new LongReader();
            private InetSocketAddress address;
            private URI jarUrl;
            private String className;
            private long startRange = -1;
            private long endRange = -1;
            private long calculationId;

            @Override
            public Reader.ProcessStatus process(ByteBuffer buffer) {
                if (state == ReaderState.DONE || state == ReaderState.ERROR) {
                    throw new IllegalStateException();
                }

                if(address == null){
                    var addressProcessState = addressReader.process(buffer);
                    if(addressProcessState != Reader.ProcessStatus.DONE){
                        return addressProcessState;
                    }
                    address = addressReader.get();
                }

                if(jarUrl == null){
                    var jarUrlProcessState = stringReader.process(buffer);
                    if(jarUrlProcessState != Reader.ProcessStatus.DONE){
                        return jarUrlProcessState;
                    }
                    try{
                        jarUrl = URI.create(stringReader.get());
                        stringReader.reset();
                    }catch (IllegalArgumentException e){
                        state = ReaderState.ERROR;
                        return ProcessStatus.ERROR;
                    }
                }

                if(className == null){
                    var classNameProcessState = stringReader.process(buffer);
                    if(classNameProcessState != Reader.ProcessStatus.DONE){
                        return classNameProcessState;
                    }
                    className = stringReader.get();
                }

                if(startRange < 0){
                    var startRangeProcessState = longReader.process(buffer);
                    if(startRangeProcessState != Reader.ProcessStatus.DONE){
                        return startRangeProcessState;
                    }
                    startRange = longReader.get();
                    if(startRange < 0){
                        state = ReaderState.ERROR;
                        return ProcessStatus.ERROR;
                    }
                    longReader.reset();
                }

                if(endRange < 0){
                    var endRangeProcessState = longReader.process(buffer);
                    if(endRangeProcessState != Reader.ProcessStatus.DONE){
                        return endRangeProcessState;
                    }
                    endRange = longReader.get();
                    if(endRange < 0){
                        state = ReaderState.ERROR;
                        return ProcessStatus.ERROR;
                    }
                    longReader.reset();
                }

                // Reading sourceAddress
                var calculationIdProcessState = longReader.process(buffer);
                if(calculationIdProcessState != Reader.ProcessStatus.DONE){
                    return calculationIdProcessState;
                }
                calculationId = longReader.get();

                state = ReaderState.DONE;
                return Reader.ProcessStatus.DONE;
            }

            @Override
            public Packet get() {
                if (state != ReaderState.DONE) {
                    throw new IllegalStateException();
                }
                return new CalculationRequestPacket(address, jarUrl, className, startRange, endRange, calculationId);
            }

            @Override
            public void reset() {
                state = ReaderState.WAITING;
                addressReader.reset();
                stringReader.reset();
                longReader.reset();
                address = null;
                jarUrl = null;
                className = null;
                startRange = -1;
                endRange = -1;
                calculationId = 0;
            }
        };
        readers.put(ID,reader);
    }

    @Override
    public Writer createWriter() {
        return new Writer() {
            enum WriterStep {ID, ADDRESS, JAR_ULR, CLASS_NAME, START_RANGE, END_RANGE, CALCULATION_ID}
            private final AddressWriter addressWriter = new AddressWriter(address);
            private final TextWriter jarUrlWriter = new TextWriter(jarUrl.toString());
            private final TextWriter classNameWriter = new TextWriter(className);
            private WriterState state = WriterState.WAITING;
            private WriterStep step = WriterStep.ID;

            @Override
            public Writer.ProcessStatus process(ByteBuffer bb) {
                if (state == WriterState.DONE) {
                    throw new IllegalStateException();
                }

                // Fill buffer with packet ID
                if (step == WriterStep.ID) {
                    if (bb.remaining() < Integer.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putInt(ID);
                    step = WriterStep.ADDRESS;
                }

                // Fill buffer with address
                if (step == WriterStep.ADDRESS) {
                    if (addressWriter.process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    step = WriterStep.JAR_ULR;
                }

                // Fill buffer with jar url
                if (step == WriterStep.JAR_ULR) {
                    if (jarUrlWriter.process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    step = WriterStep.CLASS_NAME;
                }

                // Fill buffer with class name
                if (step == WriterStep.CLASS_NAME) {
                    if (classNameWriter.process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    step = WriterStep.START_RANGE;
                }

                // Fill buffer with start range
                if (step == WriterStep.START_RANGE) {
                    if (bb.remaining() < Long.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putLong(startRange);
                    step = WriterStep.END_RANGE;
                }

                // Fill buffer with end range
                if (step == WriterStep.END_RANGE) {
                    if (bb.remaining() < Long.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putLong(endRange);
                    step = WriterStep.CALCULATION_ID;
                }

                // Fill buffer with calculation id
                if (bb.remaining() < Long.BYTES) {
                    return ProcessStatus.FULL;
                }
                bb.putLong(calculationId);

                state = WriterState.DONE;
                return Writer.ProcessStatus.DONE;
            }

            @Override
            public void reset() {
                state = WriterState.WAITING;
                step = WriterStep.ID;
                addressWriter.reset();
                jarUrlWriter.reset();
                classNameWriter.reset();
            }
        };
    }
}
