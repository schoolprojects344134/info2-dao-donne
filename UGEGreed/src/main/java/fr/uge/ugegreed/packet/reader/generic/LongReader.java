package fr.uge.ugegreed.packet.reader.generic;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

import java.nio.ByteBuffer;

public class LongReader implements Reader<Long> {
    private ReaderState state = ReaderState.WAITING;
    private final ByteBuffer internalBuffer = ByteBuffer.allocate(Long.BYTES);
    private long value;

    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == ReaderState.DONE || state == ReaderState.ERROR) {
            throw new IllegalStateException();
        }
        buffer.flip();
        try {
            if (buffer.remaining() <= internalBuffer.remaining()) {
                internalBuffer.put(buffer);
            } else {
                var oldLimit = buffer.limit();
                buffer.limit(internalBuffer.remaining());
                internalBuffer.put(buffer);
                buffer.limit(oldLimit);
            }
        } finally {
            buffer.compact();
        }
        if (internalBuffer.hasRemaining()) {
            return ProcessStatus.REFILL;
        }
        state = ReaderState.DONE;
        internalBuffer.flip();
        value = internalBuffer.getLong();
        return ProcessStatus.DONE;
    }

    @Override
    public Long get() {
        if (state != ReaderState.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    @Override
    public void reset() {
        state = ReaderState.WAITING;
        internalBuffer.clear();
    }
}