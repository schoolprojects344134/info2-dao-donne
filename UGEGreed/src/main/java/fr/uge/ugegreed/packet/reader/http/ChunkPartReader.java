package fr.uge.ugegreed.packet.reader.http;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

import java.nio.ByteBuffer;

public class ChunkPartReader implements Reader<ByteBuffer> {
  private ReaderState state = ReaderState.WAITING;
  private LineCRLFReader lineReader = new LineCRLFReader();
  private ByteBuffer resultBuffer;
  private int byteCount = -1;
  private boolean finishedReading;
  private boolean lastChunk;

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    if (state == ReaderState.DONE || state == ReaderState.ERROR) {
      throw new IllegalStateException();
    }
    if (byteCount < 0) {
      var lineReaderProcess = lineReader.process(bb);
      if (lineReaderProcess != ProcessStatus.DONE) {
        if (lineReaderProcess == ProcessStatus.ERROR) {
          state = ReaderState.ERROR;
        }
        return lineReaderProcess;
      }
      byteCount = Integer.parseInt(lineReader.get(), 16);

      if (byteCount == 0) {
        lineReader.reset();
        lineReaderProcess = lineReader.process(bb);
        if (lineReaderProcess != ProcessStatus.DONE) {
          if (lineReaderProcess == ProcessStatus.ERROR) {
            state = ReaderState.ERROR;
          }
          return lineReaderProcess;
        }

        if (!lineReader.get().equals("")) {
          state = ReaderState.ERROR;
          return ProcessStatus.ERROR;
        }

        state = ReaderState.DONE;
        lastChunk = true;
        return ProcessStatus.DONE;
      }
      
      resultBuffer = ByteBuffer.allocate(byteCount);
    }

    if(!finishedReading) {
      bb.flip(); // mode lecture
      try {
        if (bb.remaining() <= byteCount) {
          resultBuffer.put(bb);
        } else {
          var oldLimit = bb.limit();
          bb.limit(byteCount);
          resultBuffer.put(bb);
          bb.limit(oldLimit);
        }
      } finally {
        bb.compact();// mode ecriture
      }

      if (resultBuffer.position() < byteCount) {
        return ProcessStatus.REFILL;
      }
      lineReader.reset();
    }
    
    // Dump next CR LF
    var lineReaderProcess = lineReader.process(bb);
    if (lineReaderProcess != ProcessStatus.DONE) {
      if (lineReaderProcess == ProcessStatus.ERROR) {
        state = ReaderState.ERROR;
      }
      return lineReaderProcess;
    }
    
    state = ReaderState.DONE;
    return ProcessStatus.DONE;
  }

  @Override
  public ByteBuffer get() {
    if (state != ReaderState.DONE) {
      throw new IllegalStateException();
    }
    return resultBuffer;
  }

  @Override
  public void reset() {
    state = ReaderState.WAITING;
    byteCount = -1;
    resultBuffer = null;
    lineReader.reset();
    lastChunk = false;
    finishedReading = false;
  }
  
  public boolean isLastChunk() {
    return lastChunk;
  }

}