package fr.uge.ugegreed.packet.writer;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class UTF8Writer implements Writer {
    // TODO replace 1024 by STRING_BUFFER_SIZE from reader
    private static final int MAXIMUM_UTF8_CHARACTER_BYTES = 4;
    private static final int MINIMUM_SEQUENCE_LENGTH = (TextWriter.TEXT_BUFFER_SIZE / MAXIMUM_UTF8_CHARACTER_BYTES) - Integer.BYTES;
    private static final int MAXIMUM_SEQUENCE_LENGTH = TextWriter.TEXT_BUFFER_SIZE;
    private static final Charset UTF8 = StandardCharsets.UTF_8;
    private WriterState state = WriterState.WAITING;
    private final String text;
    private int position;


    public UTF8Writer(String text){
        this.text = text;
    }

    @Override
    public ProcessStatus process(ByteBuffer bb) {
        if(state == WriterState.DONE){
            throw new IllegalStateException();
        }

        //TODO Maybe it could be optimized, fill buffer when bb.remaining() > MAXIMUM_UTF8_CHARACTER_BYTES and bb.remaining() < MAXIMUM_SEQUENCE_LENGTH
        while (position < text.length()){
            if(bb.remaining() < MAXIMUM_SEQUENCE_LENGTH){
                return ProcessStatus.FULL;
            }
            var endStringPosition = 0;
            if(position + MINIMUM_SEQUENCE_LENGTH >= text.length()){
                endStringPosition = text.length();
            }else{
                endStringPosition = position + MINIMUM_SEQUENCE_LENGTH;
            }

            var textSequence = text.substring(position, endStringPosition);
            var encodedTextSequence = UTF8.encode(textSequence);
            bb.putInt(encodedTextSequence.remaining()).put(encodedTextSequence);

            position = endStringPosition;
        }

        state = WriterState.DONE;
        return ProcessStatus.DONE;
    }

    @Override
    public void reset() {
        state = WriterState.WAITING;
        position = 0;
    }
}
