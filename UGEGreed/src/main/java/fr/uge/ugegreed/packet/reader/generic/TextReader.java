package fr.uge.ugegreed.packet.reader.generic;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.writer.TextWriter;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class TextReader implements Reader<String> {
    private ReaderState state = ReaderState.WAITING;
    private final IntReader intReader = new IntReader();
    private final UTF8Reader stringReader = new UTF8Reader();
    private int textLength = -1;
    private StringBuilder contentBuilder = new StringBuilder();

    // TODO check before use that capacity is not negative
    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == ReaderState.DONE || state == ReaderState.ERROR) {
            throw new IllegalStateException();
        }

        // Read text length
        if(textLength < 0){
            var processIntState = intReader.process(buffer);
            if(processIntState != ProcessStatus.DONE){
                return processIntState;
            }
            textLength = intReader.get();
            if(textLength < 0){
                state = ReaderState.ERROR;
                return ProcessStatus.ERROR;
            }
        }

        while (textLength > 0){
            var processStringState = stringReader.process(buffer);
            if(processStringState != ProcessStatus.DONE){
                return processStringState;
            }
            var stringResult = stringReader.get();
            textLength -= stringResult.length();
            contentBuilder.append(stringResult);
            stringReader.reset();
        }

        state = ReaderState.DONE;
        return ProcessStatus.DONE;
    }

    @Override
    public String get() {
        if (state != ReaderState.DONE) {
            throw new IllegalStateException();
        }
        return contentBuilder.toString();
    }

    @Override
    public void reset() {
        state = ReaderState.WAITING;
        intReader.reset();
        textLength = -1;
        contentBuilder = new StringBuilder();
    }
}