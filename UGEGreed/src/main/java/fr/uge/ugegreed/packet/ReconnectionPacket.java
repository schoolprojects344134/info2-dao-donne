package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.generic.AddressReader;
import fr.uge.ugegreed.packet.reader.generic.LongReader;
import fr.uge.ugegreed.packet.writer.AddressWriter;
import fr.uge.ugegreed.packet.writer.TextWriter;
import fr.uge.ugegreed.packet.writer.Writer;
import fr.uge.ugegreed.packet.writer.WriterState;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public record ReconnectionPacket(InetSocketAddress parentDestinationAddress, List<InetSocketAddress> childrenAddresses, long childTotalWeight) implements Packet {
    private static final int ID = 15;
    static void createPacketReader(Map<Integer, Reader<Packet>> readers){
        var reader = new Reader<Packet>() {
            private ReaderState state = ReaderState.WAITING;
            private final AddressReader addressReader = new AddressReader();
            private final LongReader longReader = new LongReader();
            private InetSocketAddress parentDestinationAddress;
            private long childrenAddressCount = -1;
            private ArrayList<InetSocketAddress> childrenAddresses = new ArrayList<>();
            private long childTotalWeight;

            @Override
            public Reader.ProcessStatus process(ByteBuffer buffer) {
                if (state == ReaderState.DONE || state == ReaderState.ERROR) {
                    throw new IllegalStateException();
                }

                // Reading destination parent sourceAddress
                if(parentDestinationAddress == null){
                    var destinationParentAddressProcessState = addressReader.process(buffer);
                    if(destinationParentAddressProcessState != Reader.ProcessStatus.DONE){
                        return destinationParentAddressProcessState;
                    }
                    parentDestinationAddress = addressReader.get();
                    addressReader.reset();
                }

                if(childrenAddressCount < 0){
                    var childrenAddressCountProcessState = longReader.process(buffer);
                    if(childrenAddressCountProcessState != Reader.ProcessStatus.DONE){
                        return childrenAddressCountProcessState;
                    }
                    childrenAddressCount = longReader.get();
                    if(childrenAddressCount < 0){
                        state = ReaderState.ERROR;
                        return ProcessStatus.ERROR;
                    }
                    longReader.reset();
                }

                // Reading children addresses
                while (childrenAddressCount > 0){
                    var childAddressProcessState = addressReader.process(buffer);
                    if(childAddressProcessState != Reader.ProcessStatus.DONE){
                        return childAddressProcessState;
                    }
                    childrenAddresses.add(addressReader.get());
                    addressReader.reset();
                    childrenAddressCount--;
                }

                // Reading child total weight
                var childTotalWeightProcessState = longReader.process(buffer);
                if(childTotalWeightProcessState != Reader.ProcessStatus.DONE){
                    return childTotalWeightProcessState;
                }
                childTotalWeight = longReader.get();
                if(childTotalWeight < 0){
                    state = ReaderState.ERROR;
                    return ProcessStatus.ERROR;
                }

                state = ReaderState.DONE;
                return Reader.ProcessStatus.DONE;
            }

            @Override
            public Packet get() {
                if (state != ReaderState.DONE) {
                    throw new IllegalStateException();
                }
                return new ReconnectionPacket(parentDestinationAddress, childrenAddresses, childTotalWeight);
            }

            @Override
            public void reset() {
                state = ReaderState.WAITING;
                addressReader.reset();
                longReader.reset();
                parentDestinationAddress = null;
                childrenAddresses = new ArrayList<>();
                childrenAddressCount = -1;
            }
        };
        readers.put(ID,reader);
    }

    @Override
    public Writer createWriter() {
        return new Writer() {
            enum WriterStep {ID, DESTINATION_PARENT_ADDRESS, CHILDREN_ADDRESSES_COUNT,  CHILDREN_ADDRESSES, CHILD_TOTAL_WEIGHT } // TODO refactor with reader processing step
            private final AddressWriter parentDestinationAddressWriter = new AddressWriter(parentDestinationAddress);
            private final List<AddressWriter> childrenAddressesWriters = childrenAddresses.stream().map(AddressWriter::new).toList();
            private final int childrenAddressesCount = childrenAddresses.size();  // TODO in the rfc we have a long so we need to find a solution, (UPDATE) better send 2 or more request in that case
            private WriterState state = WriterState.WAITING;
            private WriterStep step = WriterStep.ID;
            private int childrenAddressesListPosition;

            @Override
            public Writer.ProcessStatus process(ByteBuffer bb) {
                if (state == WriterState.DONE) {
                    throw new IllegalStateException();
                }

                // Fill buffer with packet ID
                if (step == WriterStep.ID) {
                    if (bb.remaining() < Integer.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putInt(ID);
                    step = WriterStep.DESTINATION_PARENT_ADDRESS;
                }

                // Fill buffer with parent destination address
                if (step == WriterStep.DESTINATION_PARENT_ADDRESS) {
                    if (parentDestinationAddressWriter.process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    step = WriterStep.CHILDREN_ADDRESSES_COUNT;
                }

                if(step == WriterStep.CHILDREN_ADDRESSES_COUNT){
                    if (bb.remaining() < Long.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putLong(childrenAddressesWriters.size());
                    step = WriterStep.CHILDREN_ADDRESSES;
                }

                // Fill buffer with children addresses
                if (step == WriterStep.CHILDREN_ADDRESSES) {
                    // Fill buffer with calculation responses
                    while (childrenAddressesListPosition < childrenAddressesCount){
                        if(childrenAddressesWriters.get(childrenAddressesListPosition).process(bb) == ProcessStatus.FULL) {
                            return ProcessStatus.FULL;
                        }
                        childrenAddressesListPosition++;
                    }
                    step = WriterStep.CHILD_TOTAL_WEIGHT;
                }

                // Fill buffer with calculation id
                if (bb.remaining() < Long.BYTES) {
                    return ProcessStatus.FULL;
                }
                bb.putLong(childTotalWeight);

                state = WriterState.DONE;
                return Writer.ProcessStatus.DONE;
            }

            @Override
            public void reset() {
                state = WriterState.WAITING;
                step = WriterStep.ID;
                parentDestinationAddressWriter.reset();
                childrenAddressesWriters.forEach(AddressWriter::reset);
                childrenAddressesListPosition = 0;
            }
        };
    }
}
