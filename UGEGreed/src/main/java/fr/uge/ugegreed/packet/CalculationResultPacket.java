package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.generic.*;
import fr.uge.ugegreed.packet.writer.AddressWriter;
import fr.uge.ugegreed.packet.writer.TextWriter;
import fr.uge.ugegreed.packet.writer.Writer;
import fr.uge.ugegreed.packet.writer.WriterState;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public record CalculationResultPacket(InetSocketAddress destinationAddress, InetSocketAddress sourceAddress, long calculationId, List<String> calculationResponses) implements Packet {
    static final int ID = 12;
    static void createPacketReader(Map<Integer, Reader<Packet>> readers){
        var reader = new Reader<Packet>() {
            enum ProcessingStep { ADDRESS_DESTINATION, ADDRESS_SOURCE, CALCULATION_ID, CALCULATION_RESPONSE_COUNT, CALCULATION_RESPONSE }
            private ReaderState state = ReaderState.WAITING;
            private ProcessingStep step = ProcessingStep.ADDRESS_DESTINATION;
            private final AddressReader addressReader = new AddressReader();
            private final LongReader longReader = new LongReader();
            private final IntReader intReader = new IntReader();
            private InetSocketAddress destinationAddress;
            private InetSocketAddress sourceAddress;
            private long calculationId;
            private long responseCount;
            private ArrayList<String> calculationResponses = new ArrayList<>();
            private final TextReader stringReader = new TextReader();

            @Override
            public Reader.ProcessStatus process(ByteBuffer buffer) {
                if (state == ReaderState.DONE || state == ReaderState.ERROR) {
                    throw new IllegalStateException();
                }

                if(step == ProcessingStep.ADDRESS_DESTINATION){
                    // TODO replacing all var declaration by a single var processState ?
                    var addressDestinationProcessState = addressReader.process(buffer);
                    if(addressDestinationProcessState != Reader.ProcessStatus.DONE){
                        return addressDestinationProcessState;
                    }
                    destinationAddress = addressReader.get();
                    addressReader.reset();
                    step = ProcessingStep.ADDRESS_SOURCE;
                }

                if(step == ProcessingStep.ADDRESS_SOURCE){
                    var addressSourceProcessState = addressReader.process(buffer);
                    if(addressSourceProcessState != Reader.ProcessStatus.DONE){
                        return addressSourceProcessState;
                    }
                    sourceAddress = addressReader.get();
                    step = ProcessingStep.CALCULATION_ID;
                }

                if(step == ProcessingStep.CALCULATION_ID){
                    var calculationIdProcessState = longReader.process(buffer);
                    if(calculationIdProcessState != Reader.ProcessStatus.DONE){
                        return calculationIdProcessState;
                    }
                    calculationId = longReader.get();
                    longReader.reset();
                    step = ProcessingStep.CALCULATION_RESPONSE_COUNT;
                }

                // Reading response count
                if(step == ProcessingStep.CALCULATION_RESPONSE_COUNT){
                    var responseCountProcessState = longReader.process(buffer);
                    if(responseCountProcessState != Reader.ProcessStatus.DONE){
                        return responseCountProcessState;
                    }
                    responseCount = longReader.get();
                    if(responseCount < 0){
                        state = ReaderState.ERROR;
                        return ProcessStatus.ERROR;
                    }
                    step = ProcessingStep.CALCULATION_RESPONSE;
                }

                // Reading next response length
                while (responseCount > 0){
                    var calculationResponseProcessState = stringReader.process(buffer);
                    if(calculationResponseProcessState != Reader.ProcessStatus.DONE){
                        return calculationResponseProcessState;
                    }
                    // TODO find a solution when we have more than Integer.BYTES responses to manage Long.BYTES responses (Writer must send 2 or more request when it has more then Integer.BYTES responses see below) or parse Long in multiple Interger.MAX_VALUE and create multiple list
                    calculationResponses.add(stringReader.get());
                    stringReader.reset();
                    responseCount--;
                }
                state = ReaderState.DONE;
                return Reader.ProcessStatus.DONE;
            }

            @Override
            public Packet get() {
                if (state != ReaderState.DONE) {
                    throw new IllegalStateException();
                }
                return new CalculationResultPacket(destinationAddress,sourceAddress,calculationId,calculationResponses);
            }

            @Override
            public void reset() {
                state = ReaderState.WAITING;
                step = ProcessingStep.ADDRESS_DESTINATION;
                addressReader.reset();
                longReader.reset();
                intReader.reset();
                stringReader.reset();
                calculationResponses = new ArrayList<>();
            }
        };
        readers.put(ID,reader);
    }

    @Override
    public Writer createWriter() {
        return new Writer() {
            enum WriterStep {ID, DESTINATION_ADDRESS, SOURCE_ADDRESS, CALCULATION_ID, CALCULATION_RESPONSE_COUNT, CALCULATION_RESPONSE } // TODO refactor with reader processing step
            private final AddressWriter addressDestinationWriter = new AddressWriter(destinationAddress);
            private final AddressWriter addressSourceWriter = new AddressWriter(sourceAddress);
            private final List<TextWriter> calculationResponsesWriters = calculationResponses.stream().map(TextWriter::new).toList();
            private final int calculationResponseCount = calculationResponses.size();  // TODO in the rfc we have a long so we need to find a solution, (UPDATE) better send 2 or more request in that case
            private WriterState state = WriterState.WAITING;
            private WriterStep step = WriterStep.ID;
            private int calculationListPosition;

            @Override
            public Writer.ProcessStatus process(ByteBuffer bb) {
                if (state == WriterState.DONE) {
                    throw new IllegalStateException();
                }

                // Fill buffer with packet ID
                if (step == WriterStep.ID) {
                    if (bb.remaining() < Integer.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putInt(ID);
                    step = WriterStep.DESTINATION_ADDRESS;
                }

                // Fill buffer with destination address
                if (step == WriterStep.DESTINATION_ADDRESS) {
                    if (addressDestinationWriter.process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    step = WriterStep.SOURCE_ADDRESS;
                }

                // Fill buffer with source address
                if (step == WriterStep.SOURCE_ADDRESS) {
                    if (addressSourceWriter.process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    step = WriterStep.CALCULATION_ID;
                }

                // Fill buffer with calculation id
                if (step == WriterStep.CALCULATION_ID) {
                    if (bb.remaining() < Long.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putLong(calculationId);
                    step = WriterStep.CALCULATION_RESPONSE_COUNT;
                }


                // Fill buffer with calculation count
                if (step == WriterStep.CALCULATION_RESPONSE_COUNT) {
                    if (bb.remaining() < Long.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putLong(calculationResponseCount);
                    step = WriterStep.CALCULATION_RESPONSE;
                }

                // Fill buffer with calculation responses
                while (calculationListPosition < calculationResponseCount){
                    if(calculationResponsesWriters.get(calculationListPosition).process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    calculationListPosition++;
                }

                state = WriterState.DONE;
                return Writer.ProcessStatus.DONE;
            }

            @Override
            public void reset() {
                state = WriterState.WAITING;
                step = WriterStep.ID;
                addressDestinationWriter.reset();
                addressSourceWriter.reset();
                calculationListPosition = 0;
                calculationResponsesWriters.forEach(TextWriter::reset);
            }
        };
    }
}
