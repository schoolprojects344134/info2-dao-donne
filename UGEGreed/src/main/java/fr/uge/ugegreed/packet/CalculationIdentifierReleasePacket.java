package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.generic.AddressReader;
import fr.uge.ugegreed.packet.reader.generic.LongReader;
import fr.uge.ugegreed.packet.writer.AddressWriter;
import fr.uge.ugegreed.packet.writer.Writer;
import fr.uge.ugegreed.packet.writer.WriterState;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Map;

public record CalculationIdentifierReleasePacket(long calculationIdentifier) implements Packet {
    static final int ID = 19;
    static void createPacketReader(Map<Integer, Reader<Packet>> readers){
        var reader = new Reader<Packet>() {
            private ReaderState state = ReaderState.WAITING;
            private final LongReader longReader = new LongReader();
            private long calculationIdentifier = -1;

            @Override
            public Reader.ProcessStatus process(ByteBuffer buffer) {
                if (state == ReaderState.DONE || state == ReaderState.ERROR) {
                    throw new IllegalStateException();
                }

                // Reading destination sourceAddress
                var sourceAddressProcessState = longReader.process(buffer);
                if(sourceAddressProcessState != Reader.ProcessStatus.DONE){
                    return sourceAddressProcessState;
                }
                calculationIdentifier = longReader.get();

                state = ReaderState.DONE;
                return Reader.ProcessStatus.DONE;
            }

            @Override
            public Packet get() {
                if (state != ReaderState.DONE) {
                    throw new IllegalStateException();
                }
                return new CalculationIdentifierReleasePacket(calculationIdentifier);
            }

            @Override
            public void reset() {
                state = ReaderState.WAITING;
                longReader.reset();
                calculationIdentifier = -1;
            }
        };
        readers.put(ID,reader);
    }

    @Override
    public Writer createWriter() {
        return new Writer() {
            enum WriterStep {ID, CALCULATION_IDENTIFIER}
            private WriterState state = WriterState.WAITING;
            private WriterStep step = WriterStep.ID;

            @Override
            public Writer.ProcessStatus process(ByteBuffer bb) {
                if (state == WriterState.DONE) {
                    throw new IllegalStateException();
                }

                // Fill buffer with packet ID
                if (step == WriterStep.ID) {
                    if (bb.remaining() < Integer.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putInt(ID);
                    step = WriterStep.CALCULATION_IDENTIFIER;
                }

                if(step == WriterStep.CALCULATION_IDENTIFIER){
                    // Fill buffer with network weight
                    if (bb.remaining() < Long.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putLong(calculationIdentifier);
                }

                state = WriterState.DONE;
                return Writer.ProcessStatus.DONE;
            }

            @Override
            public void reset() {
                state = WriterState.WAITING;
                step = WriterStep.ID;
            }
        };
    }
}
