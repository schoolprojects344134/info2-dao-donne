package fr.uge.ugegreed.packet.reader.generic;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


// TODO to be factorised with UTF8Reader (UPDATE): Better add this directly to the address writer as we don't need it for anything else
public class ASCIIReader implements Reader<String>{
    private final Charset ASCII = StandardCharsets.US_ASCII;
    private ReaderState state = ReaderState.WAITING;
    private final IntReader intReader = new IntReader();
    private final ByteBuffer internalBufferString = ByteBuffer.allocate(10_000);
    private int byteCount = -1;
    private StringBuilder contentBuilder = new StringBuilder();

    // TODO check before use that capacity is not negative
    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == ReaderState.DONE || state == ReaderState.ERROR) {
            throw new IllegalStateException();
        }

        // Read text byte count
        if(byteCount < 0){
            var processIntState = intReader.process(buffer);
            if(processIntState != ProcessStatus.DONE){
                return processIntState;
            }
            byteCount = intReader.get();
            if(byteCount < 0){
                state = ReaderState.ERROR;
                return ProcessStatus.ERROR;
            }
        }

        buffer.flip();
        try {
            if (buffer.remaining() <= byteCount) {
                internalBufferString.put(buffer);
            } else {
                var oldLimit = buffer.limit();
                buffer.limit(byteCount);
                internalBufferString.put(buffer);
                buffer.limit(oldLimit);
            }
        } finally {
            buffer.compact();
        }

        internalBufferString.flip();
        contentBuilder.append(ASCII.decode(internalBufferString));
        byteCount -= internalBufferString.position();

        // TODO byteCount < 0 error
        if(byteCount < 0){
            state = ReaderState.ERROR;
            return ProcessStatus.ERROR;
        }
        if (byteCount > 0) {
            internalBufferString.clear();
            return ProcessStatus.REFILL;
        }

        state = ReaderState.DONE;
        return ProcessStatus.DONE;
    }

    @Override
    public String get() {
        if (state != ReaderState.DONE) {
            throw new IllegalStateException();
        }
        return contentBuilder.toString();
    }

    @Override
    public void reset() {
        state = ReaderState.WAITING;
        intReader.reset();
        internalBufferString.clear();
        byteCount = -1;
        contentBuilder = new StringBuilder();
    }
}
