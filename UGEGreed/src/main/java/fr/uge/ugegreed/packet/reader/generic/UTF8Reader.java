package fr.uge.ugegreed.packet.reader.generic;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class UTF8Reader implements Reader<String> {
    public static final int BUFFER_STRING_SIZE = 1024; // TODO use string writer buffer size
    private final Charset UTF8 = StandardCharsets.UTF_8;
    private ReaderState state = ReaderState.WAITING;
    private final IntReader intReader = new IntReader();
    private final ByteBuffer internalBufferString = ByteBuffer.allocate(BUFFER_STRING_SIZE); // 1024
    private int byteCount = -1;
    private StringBuilder contentBuilder = new StringBuilder();

    // TODO check before use that capacity is not negative
    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == ReaderState.DONE || state == ReaderState.ERROR) {
            throw new IllegalStateException();
        }

        // Read text byte count
        if(byteCount < 0){
            var processIntState = intReader.process(buffer);
            if(processIntState != ProcessStatus.DONE){
                return processIntState;
            }
            byteCount = intReader.get();
            if(byteCount < 0){
                state = ReaderState.ERROR;
                return ProcessStatus.ERROR;
            }
        }

        buffer.flip();
        try {
            if (buffer.remaining() <= byteCount) {
                internalBufferString.put(buffer);
            } else {
                var oldLimit = buffer.limit();
                buffer.limit(buffer.position() + byteCount);
                internalBufferString.put(buffer);
                buffer.limit(oldLimit);
            }
        } finally {
            buffer.compact();
        }

        internalBufferString.flip();
        contentBuilder.append(UTF8.decode(internalBufferString));
        byteCount -= internalBufferString.position();

        if (byteCount > 0) {
            internalBufferString.clear();
            return ProcessStatus.REFILL;
        }

        state = ReaderState.DONE;
        return ProcessStatus.DONE;
    }

    @Override
    public String get() {
        if (state != ReaderState.DONE) {
            throw new IllegalStateException();
        }
        return contentBuilder.toString();
    }

    @Override
    public void reset() {
        state = ReaderState.WAITING;
        intReader.reset();
        internalBufferString.clear();
        byteCount = -1;
        contentBuilder = new StringBuilder();
    }
}