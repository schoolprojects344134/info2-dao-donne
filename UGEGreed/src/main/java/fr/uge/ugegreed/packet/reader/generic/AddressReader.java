package fr.uge.ugegreed.packet.reader.generic;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public class AddressReader implements Reader<InetSocketAddress> {
    private ReaderState state = ReaderState.WAITING;
    private final ASCIIReader addressReader = new ASCIIReader();
    private final IntReader portReader = new IntReader();
    private String address;
    private int port = -1;
    private InetSocketAddress resultAddress;

    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == ReaderState.DONE || state == ReaderState.ERROR) {
            throw new IllegalStateException();
        }

        // Reading sourceAddress
        if(address == null){
            var addressProcessState = addressReader.process(buffer);
            if(addressProcessState != ProcessStatus.DONE){
                return addressProcessState;
            }
            address = addressReader.get();
        }

        // Reading port
        var portProcessState = portReader.process(buffer);
        if(portProcessState != ProcessStatus.DONE){
            return portProcessState;
        }
        port = portReader.get();

        // TODO try to do not have an exception
        try{
            resultAddress = new InetSocketAddress(address, port);
            state = ReaderState.DONE;
            return ProcessStatus.DONE;
        }catch (IllegalArgumentException e){
            state = ReaderState.ERROR;
            return ProcessStatus.ERROR;
        }
    }

    @Override
    public InetSocketAddress get() {
        if (state != ReaderState.DONE) {
            throw new IllegalStateException();
        }
        return resultAddress;
    }

    @Override
    public void reset() {
        state = ReaderState.WAITING;
        addressReader.reset();
        portReader.reset();
        address = null;
        port = -1;
    }
}