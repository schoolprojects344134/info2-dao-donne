package fr.uge.ugegreed.packet.reader;

public enum ReaderState {
    DONE, WAITING, ERROR
}
