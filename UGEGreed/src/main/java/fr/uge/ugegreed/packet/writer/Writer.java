package fr.uge.ugegreed.packet.writer;

import fr.uge.ugegreed.packet.reader.Reader;

import java.nio.ByteBuffer;

public interface Writer {
    enum ProcessStatus { DONE, FULL };
    Writer.ProcessStatus process(ByteBuffer bb);
    void reset();
}
