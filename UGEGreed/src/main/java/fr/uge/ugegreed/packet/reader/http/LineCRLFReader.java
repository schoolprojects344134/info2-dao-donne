package fr.uge.ugegreed.packet.reader.http;

import java.nio.ByteBuffer;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

public class LineCRLFReader implements Reader<String>{
  private ReaderState state = ReaderState.WAITING;
  private String value;
  private StringBuilder stringBuilder = new StringBuilder();

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    if(state == ReaderState.DONE || state == ReaderState.ERROR) {
      throw new IllegalStateException();
    }
    bb.flip(); 
    var lineEnd = false;
    for(;;){
        if(!bb.hasRemaining()){
          return ProcessStatus.REFILL;
        }
        
        var readChar = (char)bb.get();
        if(lineEnd && readChar == '\n'){
            bb.compact();
            break;
        }
        stringBuilder.append(readChar);
        lineEnd = readChar == '\r';
    }
    var result = stringBuilder.toString();
    value = result.substring(0,result.length() - 1 );
    state = ReaderState.DONE;
    return ProcessStatus.DONE;
  }

  @Override
  public String get() {
    if(state != ReaderState.DONE) {
      throw new IllegalStateException();
    }
    return value;
  }

  @Override
  public void reset() {
    state = ReaderState.WAITING;
    stringBuilder = new StringBuilder();
    value = null;
  }

}
