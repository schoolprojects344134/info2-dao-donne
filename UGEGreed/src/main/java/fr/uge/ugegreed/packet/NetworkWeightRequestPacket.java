package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.generic.AddressReader;
import fr.uge.ugegreed.packet.writer.AddressWriter;
import fr.uge.ugegreed.packet.writer.Writer;
import fr.uge.ugegreed.packet.writer.WriterState;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Map;

public record NetworkWeightRequestPacket(InetSocketAddress sourceAddress) implements Packet {
    private static final int ID = 8;
    static void createPacketReader(Map<Integer, Reader<Packet>> readers){
        var reader = new Reader<Packet>() {
            private ReaderState state = ReaderState.WAITING;
            private final AddressReader addressReader = new AddressReader();
            private InetSocketAddress sourceAddress;

            @Override
            public Reader.ProcessStatus process(ByteBuffer buffer) {
                if (state == ReaderState.DONE || state == ReaderState.ERROR) {
                    throw new IllegalStateException();
                }

                // Reading sourceAddress
                var addressProcessState = addressReader.process(buffer);
                if(addressProcessState != Reader.ProcessStatus.DONE){
                    return addressProcessState;
                }

                sourceAddress = addressReader.get();
                state = ReaderState.DONE;
                return Reader.ProcessStatus.DONE;
            }

            @Override
            public Packet get() {
                if (state != ReaderState.DONE) {
                    throw new IllegalStateException();
                }
                return new NetworkWeightRequestPacket(sourceAddress);
            }

            @Override
            public void reset() {
                state = ReaderState.WAITING;
                addressReader.reset();
            }
        };
        readers.put(ID,reader);
    }

    @Override
    public Writer createWriter() {
        return new Writer() {
            enum WriterStep {ID, ADDRESS}
            private final AddressWriter addressWriter = new AddressWriter(sourceAddress);
            private WriterState state = WriterState.WAITING;
            private WriterStep step = WriterStep.ID;

            @Override
            public Writer.ProcessStatus process(ByteBuffer bb) {
                if (state == WriterState.DONE) {
                    throw new IllegalStateException();
                }

                // Fill buffer with packet ID
                if (step == WriterStep.ID) {
                    if (bb.remaining() < Integer.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putInt(ID);
                    step = WriterStep.ADDRESS;
                }

                // Fill buffer with the ip sourceAddress
                if (addressWriter.process(bb) == ProcessStatus.FULL) {
                    return ProcessStatus.FULL;
                }

                state = WriterState.DONE;
                return Writer.ProcessStatus.DONE;
            }

            @Override
            public void reset() {
                state = WriterState.WAITING;
                step = WriterStep.ID;
                addressWriter.reset();
            }
        };
    }
}
