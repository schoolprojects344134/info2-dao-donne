package fr.uge.ugegreed.packet.writer;

public enum WriterState {
    DONE, REMAINING, WAITING
}
