package fr.uge.ugegreed.packet.writer;

import fr.uge.ugegreed.network.Context;

import java.nio.ByteBuffer;

public class TextWriter implements Writer {
    public static final int TEXT_BUFFER_SIZE = Context.CONTEXT_BUFFER_SIZE / 5;
    private enum WriterStep { TEXT_LENGTH , CONTENT}
    private final int textLength;
    private final UTF8Writer stringWriter;
    private WriterStep step = WriterStep.TEXT_LENGTH;
    private WriterState state = WriterState.WAITING;

    public TextWriter(String text){
        this.textLength = text.length();
        this.stringWriter = new UTF8Writer(text);
    }

    @Override
    public ProcessStatus process(ByteBuffer bb) {
        if(state == WriterState.DONE){
            throw new IllegalStateException();
        }

        // Fill buffer with text length
        if(step == WriterStep.TEXT_LENGTH){
            if(bb.remaining() < Integer.BYTES){
                return ProcessStatus.FULL;
            }
            bb.putInt(textLength);
            step = WriterStep.CONTENT;
        }

        // Fill buffer with UTF8 content
        var processStringWriterStatus = stringWriter.process(bb);
        if(processStringWriterStatus == ProcessStatus.FULL){
            return processStringWriterStatus;
        }

        state = WriterState.DONE;
        return ProcessStatus.DONE;
    }

    @Override
    public void reset() {
        state = WriterState.WAITING;
        step = WriterStep.TEXT_LENGTH;
        stringWriter.reset();
    }
}
