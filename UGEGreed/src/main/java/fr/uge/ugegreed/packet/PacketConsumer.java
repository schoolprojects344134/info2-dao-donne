package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.network.Context;
import fr.uge.ugegreed.packet.Packet;

@FunctionalInterface
public interface PacketConsumer {
    void process(Context context, Packet packet);
}
