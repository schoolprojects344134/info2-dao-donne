package fr.uge.ugegreed.packet.reader.generic;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class StringReader implements Reader<String> {
    public static final int BUFFER_STRING_SIZE = 1024;
    private final Charset UTF8 = StandardCharsets.UTF_8;
    private ReaderState state = ReaderState.WAITING;
    private final IntReader intReader = new IntReader();
    private final ByteBuffer internalBufferString = ByteBuffer.allocate(BUFFER_STRING_SIZE);
    private int byteCount = -1;
    private int subSequenceByteCount = -1;
    private StringBuilder contentBuilder = new StringBuilder();

    // TODO check before use that capacity is not negative
    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == ReaderState.DONE || state == ReaderState.ERROR) {
            throw new IllegalStateException();
        }

        // Read text byte count
        if(byteCount < 0){
            var processIntState = intReader.process(buffer);
            if(processIntState != ProcessStatus.DONE){
                return processIntState;
            }
            byteCount = intReader.get();
            if(byteCount < 0){
                state = ReaderState.ERROR;
                return ProcessStatus.ERROR;
            }
            intReader.reset();
        }


        while (byteCount > 0){
            // Reading next subsequence length
            if(subSequenceByteCount < 0){
                var processIntState = intReader.process(buffer);
                if(processIntState != ProcessStatus.DONE){
                    return processIntState;
                }
                subSequenceByteCount = intReader.get();
                if(subSequenceByteCount < 0 || subSequenceByteCount > BUFFER_STRING_SIZE){
                    state = ReaderState.ERROR;
                    return ProcessStatus.ERROR;
                }
                intReader.reset();
                // Decrement byte count by the integer bytes read
                byteCount -= Integer.BYTES;
            }

            // Reading next subsequence
            buffer.flip();
            try {
                if (buffer.remaining() <= subSequenceByteCount) {
                    internalBufferString.put(buffer);
                } else {
                    var oldLimit = buffer.limit();
                    buffer.limit(subSequenceByteCount);
                    internalBufferString.put(buffer);
                    buffer.limit(oldLimit);
                }
            } finally {
                buffer.compact();
            }

            // TODO check if this is necessary
            if(internalBufferString.position() < subSequenceByteCount){
                return ProcessStatus.REFILL;
            }

            internalBufferString.flip();

            // Decrement byte count by byte count read
            byteCount -= subSequenceByteCount;

            // Add to string builder the sub-sequence
            contentBuilder.append(UTF8.decode(internalBufferString));

            internalBufferString.clear();
            subSequenceByteCount = -1;
         }

        state = ReaderState.DONE;
        return ProcessStatus.DONE;
    }

    @Override
    public String get() {
        if (state != ReaderState.DONE) {
            throw new IllegalStateException();
        }
        return contentBuilder.toString();
    }

    @Override
    public void reset() {
        state = ReaderState.WAITING;
        intReader.reset();
        internalBufferString.clear();
        byteCount = -1;
        contentBuilder = new StringBuilder();
        subSequenceByteCount = -1;
    }
}
