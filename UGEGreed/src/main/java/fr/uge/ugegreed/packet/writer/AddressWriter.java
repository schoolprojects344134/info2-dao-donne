package fr.uge.ugegreed.packet.writer;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class AddressWriter implements Writer{
    private enum WriterStep { ADDRESS_LENGTH , ADDRESS, PORT}
    private static final Charset ASCII = StandardCharsets.US_ASCII;
    private final int addressLength;
    private final ByteBuffer addressEncoded;
    private final int port;
    private WriterStep step = WriterStep.ADDRESS_LENGTH;
    private WriterState state = WriterState.WAITING;

    public AddressWriter(InetSocketAddress address){
        var ipAddress = address.getHostString();
        this.addressLength = ipAddress.length();
        this.addressEncoded = ASCII.encode(ipAddress);
        this.port = address.getPort();
    }

    @Override
    public Writer.ProcessStatus process(ByteBuffer bb) {
        if(state == WriterState.DONE){
            throw new IllegalStateException();
        }

        // Fill buffer with sourceAddress length
        if(step == WriterStep.ADDRESS_LENGTH){
            if(bb.remaining() < Integer.BYTES){
                return ProcessStatus.FULL;
            }
            bb.putInt(addressLength);
            step = WriterStep.ADDRESS;
        }

        // Fill buffer with the ip sourceAddress
        if(step == WriterStep.ADDRESS){
            if(bb.remaining() < addressEncoded.remaining()){
                var originalLimit = addressEncoded.limit();
                addressEncoded.limit(addressEncoded.position() + bb.remaining());
                bb.put(addressEncoded);
                addressEncoded.limit(originalLimit);
                return ProcessStatus.FULL;
            }else {
                bb.put(addressEncoded);
            }
            step = WriterStep.PORT;
        }

        // Fill buffer with port
        if(bb.remaining() < Integer.BYTES){
            return ProcessStatus.FULL;
        }
        bb.putInt(port);

        state = WriterState.DONE;
        return Writer.ProcessStatus.DONE;
    }

    @Override
    public void reset() {
        state = WriterState.WAITING;
        step = WriterStep.ADDRESS_LENGTH;
        addressEncoded.position(0);
    }
}
