package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;
import fr.uge.ugegreed.packet.reader.generic.AddressReader;
import fr.uge.ugegreed.packet.reader.generic.LongReader;
import fr.uge.ugegreed.packet.writer.AddressWriter;
import fr.uge.ugegreed.packet.writer.Writer;
import fr.uge.ugegreed.packet.writer.WriterState;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Map;

public record NetworkWeightResponsePacket(InetSocketAddress destinationAddress, long networkWeight) implements Packet {
    private static final int ID = 9;
    static void createPacketReader(Map<Integer, Reader<Packet>> readers){
        var reader = new Reader<Packet>() {
            private ReaderState state = ReaderState.WAITING;
            private final AddressReader addressReader = new AddressReader();
            private final LongReader longReader = new LongReader();
            private InetSocketAddress destinationAddress;
            private long networkWeight;

            @Override
            public Reader.ProcessStatus process(ByteBuffer buffer) {
                if (state == ReaderState.DONE || state == ReaderState.ERROR) {
                    throw new IllegalStateException();
                }

                // Reading sourceAddress
                if(destinationAddress == null){
                    var addressProcessState = addressReader.process(buffer);
                    if(addressProcessState != Reader.ProcessStatus.DONE){
                        return addressProcessState;
                    }
                    destinationAddress = addressReader.get();
                }

                var networkWeightProcessState = longReader.process(buffer);
                if(networkWeightProcessState != Reader.ProcessStatus.DONE){
                    return networkWeightProcessState;
                }
                networkWeight = longReader.get();
                if(networkWeight < 0){
                    state = ReaderState.ERROR;
                    return ProcessStatus.ERROR;
                }

                state = ReaderState.DONE;
                return Reader.ProcessStatus.DONE;
            }

            @Override
            public Packet get() {
                if (state != ReaderState.DONE) {
                    throw new IllegalStateException();
                }
                return new NetworkWeightResponsePacket(destinationAddress, networkWeight);
            }

            @Override
            public void reset() {
                state = ReaderState.WAITING;
                addressReader.reset();
                longReader.reset();
                destinationAddress = null;
            }
        };
        readers.put(ID,reader);
    }

    @Override
    public Writer createWriter() {
        return new Writer() {
            enum WriterStep {ID, DESTINATION_ADDRESS, NETWORK_WEIGHT }
            private final AddressWriter destinationAddressWriter = new AddressWriter(destinationAddress);
            private WriterState state = WriterState.WAITING;
            private WriterStep step = WriterStep.ID;

            @Override
            public Writer.ProcessStatus process(ByteBuffer bb) {
                if (state == WriterState.DONE) {
                    throw new IllegalStateException();
                }

                // Fill buffer with packet ID
                if (step == WriterStep.ID) {
                    if (bb.remaining() < Integer.BYTES) {
                        return ProcessStatus.FULL;
                    }
                    bb.putInt(ID);
                    step = WriterStep.DESTINATION_ADDRESS;
                }

                // Fill buffer with destination address
                if (step == WriterStep.DESTINATION_ADDRESS) {
                    if (destinationAddressWriter.process(bb) == ProcessStatus.FULL) {
                        return ProcessStatus.FULL;
                    }
                    step = WriterStep.NETWORK_WEIGHT;
                }

                // Fill buffer with network weight
                if (bb.remaining() < Long.BYTES) {
                    return ProcessStatus.FULL;
                }
                bb.putLong(networkWeight);

                state = WriterState.DONE;
                return Writer.ProcessStatus.DONE;
            }

            @Override
            public void reset() {
                state = WriterState.WAITING;
                step = WriterStep.ID;
                destinationAddressWriter.reset();
            }
        };
    }
}
