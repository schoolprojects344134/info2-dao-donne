package fr.uge.ugegreed.packet.reader.http;

import java.nio.ByteBuffer;

import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

public class ChunkReader implements Reader<ByteBuffer>{
  private ReaderState state = ReaderState.WAITING;
  private static int BUFFER_SIZE = 1024;
  private ByteBuffer resultBuffer = ByteBuffer.allocate(BUFFER_SIZE);
  private final ChunkPartReader chunkpart = new ChunkPartReader();

  private void resize(){
    var newBb = ByteBuffer.allocate(resultBuffer.capacity() * 2);
    resultBuffer = newBb.put(resultBuffer);
  }
  
  @Override
  public ProcessStatus process(ByteBuffer bb) {
    if (state == ReaderState.DONE || state == ReaderState.ERROR) {
      throw new IllegalStateException();
    }
    while(true) {
      var chunkProcess = chunkpart.process(bb);
      if (chunkProcess != ProcessStatus.DONE) {
        if (chunkProcess == ProcessStatus.ERROR) {
          state = ReaderState.ERROR;
        }
        return chunkProcess;
      }
      if(chunkpart.isLastChunk()) {
        break;
      }
      var chunk = chunkpart.get();
      while(chunk.remaining() >= resultBuffer.remaining()) {
        resize();
      }
      resultBuffer.put(chunk);
    }
    
    state = ReaderState.DONE;
    return ProcessStatus.DONE;
  }

  @Override
  public ByteBuffer get() {
    if (state != ReaderState.DONE) {
      throw new IllegalStateException();
    }
    return resultBuffer;
  }

  @Override
  public void reset() {
    state = ReaderState.WAITING;
    resultBuffer.clear();
    chunkpart.reset();
    
  }

}
