package fr.uge.ugegreed.packet.reader.http;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import fr.uge.ugegreed.client.http.HTTPException;
import fr.uge.ugegreed.client.http.HTTPHeader;
import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.reader.ReaderState;

public class HeaderReader implements Reader<HTTPHeader> {
  private ReaderState state = ReaderState.WAITING;
  private HTTPHeader header;
  private LineCRLFReader lineReader = new LineCRLFReader();
  private String response;
  private String headerContent;
  private final Map<String, String> map = new HashMap<>();

  @Override
  public ProcessStatus process(ByteBuffer bb) {
    if (state == ReaderState.DONE || state == ReaderState.ERROR) {
      throw new IllegalStateException();
    }
    if (response == null) {
      var responseProcess = lineReader.process(bb);
      if (responseProcess != ProcessStatus.DONE) {
        if (responseProcess == ProcessStatus.ERROR) {
          state = ReaderState.ERROR;
        }
        return responseProcess;
      }
      response = lineReader.get();
      lineReader.reset();
    }
    
    var headerContentProcess = lineReader.process(bb);
    if (headerContentProcess != ProcessStatus.DONE) {
      if (headerContentProcess == ProcessStatus.ERROR) {
        state = ReaderState.ERROR;
      }
      return headerContentProcess;
    }
    headerContent = lineReader.get();

    while(!headerContent.equals("")) {
      var splitContent = headerContent.split(":", 2);
      map.compute(splitContent[0],
          (__, currentValue) -> currentValue == null ? splitContent[1] : currentValue + ";" + splitContent[1]);
      lineReader.reset();
      headerContentProcess = lineReader.process(bb);
      headerContent = lineReader.get();
    }

    try {
      header = HTTPHeader.create(response, map);
    } catch (HTTPException e) {
      state = ReaderState.ERROR;
      return ProcessStatus.ERROR;
    }

    state = ReaderState.DONE;
    return ProcessStatus.DONE;
  }

  @Override
  public HTTPHeader get() {
    if (state != ReaderState.DONE) {
      throw new IllegalStateException();
    }
    return header;
  }

  @Override
  public void reset() {
    state = ReaderState.WAITING;
    lineReader = new LineCRLFReader();
    map.clear();
    header = null;
    response = null;
    headerContent = null;
  }

}
