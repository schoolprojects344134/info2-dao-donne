package fr.uge.ugegreed;

import fr.uge.ugegreed.network.Context;
import fr.uge.ugegreed.packet.writer.AddressWriter;
import fr.uge.ugegreed.packet.writer.UTF8Writer;
import fr.uge.ugegreed.packet.writer.TextWriter;
import fr.uge.ugegreed.packet.writer.Writer;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Enclosed.class)
public class WriterTest {
    public static class TextWriterTest {
        @Test
        public void testTextWriterSimple() {
            var content = """
        aaaaaaaaaaa€€€€€€€€€€€€€€€€€aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaéééééééééééééééééééaaaaaaaaaaaa
        aaaaaaaaaa€€€€€€€€€€€€€€€€€aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaéééééééééééééééééééaaaaaaaaaaaa
        aaaaaaaaaaaaaaaaaa€€€€€€€€€€€€€€€€€aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaéééééééééééééééééééaaaaaaaaaaaa
        """;
            var resultString = "";
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var textWriter = new TextWriter(content);
            switch (textWriter.process(bb)){
                case FULL -> throw new AssertionError();
                case DONE -> {
                    bb.flip();
                    var contentLength = bb.getInt();
                    assertEquals(content.length(),contentLength);
                    var originalLimit = bb.limit();
                    while (resultString.length() < contentLength){
                        var sequenceLength = bb.getInt();
                        bb.limit(bb.position() + sequenceLength);
                        resultString += StandardCharsets.UTF_8.decode(bb).toString();
                        bb.limit(originalLimit);
                    }
                    assertEquals(content, resultString);
                }
            }
        }

    }

    public static class UTF8WriterTest {
        @Test
        public void UTF8WriterWithSplit() {
            var content = "ééééséééééééééééééééééééééééééééééééééééééééééésééééééééééééééééééééééésééééééééééééééééééééééééééééééééééééééésééééééééééééééééééééééééééééééééééééséééééééééééééééééééééééééééééééééééééééséééééééééééééééééééééééééééééééééééésééééééééééééééééééééééééééééééééééééééésééééééééééééééééééééééééééééééééééééséééééééééééééééééééééééééééééééééééééééséééééééééééééééééééééééééééééééééééésééééééééééééééééééééééééééééééééééééééésééééééééééééééééééééééééééééééééééééséééééééééééééééééééééééééééééééééééééééséééééééééééééééééééééééééééééééééééésééééééééééééééééééééééééééééééééééééséééééééééééééééééééééééééééééééééééé";
            var resultString = "";
            var bb = ByteBuffer.allocate(2_000);
            var writer = new UTF8Writer(content);
            while (true){
                switch (writer.process(bb)){
                    case FULL -> {
                        bb.flip();
                        var contentLength = bb.getInt();
                        resultString += StandardCharsets.UTF_8.decode(bb).toString();
                        bb.clear();
                    }
                    case DONE -> {
                        bb.flip();
                        var contentLength = bb.getInt();
                        resultString += StandardCharsets.UTF_8.decode(bb).toString();
                        bb.clear();
                        assertEquals(content, resultString);
                        return;
                    }
                }
            }
        }
    }

    public static class AddressWriterTest {
        private static final Charset ASCII = StandardCharsets.US_ASCII;
        @Test
        public void addressWriterIPV4Simple() {
            var ipAddress = "120.0.255.120";
            var port = 1000;
            var address = new InetSocketAddress(ipAddress, port);
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var writer = new AddressWriter(address);
            switch (writer.process(bb)){
                case FULL -> {
                    throw new AssertionError();
                }
                case DONE -> {
                    bb.flip();
                    var addressLength = bb.getInt();
                    assertEquals(ipAddress.length(), addressLength);
                    var originalLimit = bb.limit();
                    bb.limit(bb.position() + addressLength);
                    var addressDecoded = ASCII.decode(bb).toString();
                    bb.limit(originalLimit);
                    assertEquals(ipAddress, addressDecoded);
                    assertEquals(port, bb.getInt());
                }
            }
        }

        @Test
        public void addressWriterIPV6Simple() {
            var ipAddressFull = "2001:0db8:3c4d:0015:0000:0000:1a2f:1a2b";
            var port = 1000;
            var address = new InetSocketAddress(ipAddressFull, port);
            var ipAddressExpected = address.getHostName();
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var writer = new AddressWriter(address);
            switch (writer.process(bb)){
                case FULL -> {
                    throw new AssertionError();
                }
                case DONE -> {
                    bb.flip();
                    var addressLength = bb.getInt();
                    assertEquals(ipAddressExpected.length(), addressLength);
                    var originalLimit = bb.limit();
                    bb.limit(bb.position() + addressLength);
                    var addressDecoded = ASCII.decode(bb).toString();
                    bb.limit(originalLimit);
                    assertEquals(ipAddressExpected, addressDecoded);
                    assertEquals(port, bb.getInt());
                }
            }
        }


        @Test
        public void addressWriterIPV4Reset() {
            var ipAddress = "120.0.255.120";
            var port = 1000;
            var address = new InetSocketAddress(ipAddress, port);
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var writer = new AddressWriter(address);
            switch (writer.process(bb)){
                case FULL -> {
                    throw new AssertionError();
                }
                case DONE -> {
                    bb.flip();
                    var addressLength = bb.getInt();
                    assertEquals(ipAddress.length(), addressLength);
                    var originalLimit = bb.limit();
                    bb.limit(bb.position() + addressLength);
                    var addressDecoded = ASCII.decode(bb).toString();
                    bb.limit(originalLimit);
                    assertEquals(ipAddress, addressDecoded);
                    assertEquals(port, bb.getInt());
                }
            }
            bb.clear();
            writer.reset();
            switch (writer.process(bb)){
                case FULL -> {
                    throw new AssertionError();
                }
                case DONE -> {
                    bb.flip();
                    var addressLength = bb.getInt();
                    assertEquals(ipAddress.length(), addressLength);
                    var originalLimit = bb.limit();
                    bb.limit(bb.position() + addressLength);
                    var addressDecoded = ASCII.decode(bb).toString();
                    bb.limit(originalLimit);
                    assertEquals(ipAddress, addressDecoded);
                    assertEquals(port, bb.getInt());
                }
            }
        }

        @Test
        public void addressWriterWithMultipleBufferFilling() {
            var ipAddress = "120.0.255.120";
            var port = 1000;
            var address = new InetSocketAddress(ipAddress, port);
            var bb = ByteBuffer.allocate(4);
            var writer = new AddressWriter(address);
            var resultIpAddress = "";
            assertEquals(Writer.ProcessStatus.FULL,  writer.process(bb.clear()));
            var addressLength = bb.flip().getInt();
            assertEquals(ipAddress.length(), addressLength);
            assertEquals(Writer.ProcessStatus.FULL,  writer.process(bb.clear()));
            resultIpAddress += ASCII.decode(bb.flip()).toString();
            assertEquals(Writer.ProcessStatus.FULL,  writer.process(bb.clear()));
            resultIpAddress += ASCII.decode(bb.flip()).toString();
            assertEquals(Writer.ProcessStatus.FULL,  writer.process(bb.clear()));
            resultIpAddress += ASCII.decode(bb.flip()).toString();
            assertEquals(Writer.ProcessStatus.FULL,  writer.process(bb.clear()));
            resultIpAddress += ASCII.decode(bb.flip()).toString();
            assertEquals(ipAddress, resultIpAddress);
            assertEquals(Writer.ProcessStatus.DONE,  writer.process(bb.clear()));
            assertEquals(port, bb.flip().getInt());
        }
    }

}
