package fr.uge.ugegreed.packet;

import fr.uge.ugegreed.network.Context;
import fr.uge.ugegreed.packet.reader.Reader;
import fr.uge.ugegreed.packet.writer.Writer;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
@RunWith(Enclosed.class)
public class ReaderTest {
    public static class PacketReaderTest {
        @Test
        public void packetReaderTestSimple() {
            var ipAddress = "127.0.0.1";
            var port = 5000;
            var inetAddress = new InetSocketAddress(ipAddress, port);
            var packet = new ConnectionPacket(inetAddress);
            var packetReader = new PacketReader();
            var packetWriter = packet.createWriter();
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            assertEquals(Writer.ProcessStatus.DONE,packetWriter.process(bb));
            assertEquals(Reader.ProcessStatus.DONE,packetReader.process(bb));
            assertEquals(packet,packetReader.get());
        }
    }


    public static class ConnectionPacketReaderTest {
        @Test
        public void packetReaderTestSimple() {
            var ipAddress = "127.0.0.1";
            var port = 5000;
            var inetAddress = new InetSocketAddress(ipAddress, port);
            var packet = new ConnectionPacket(inetAddress);
            var pr = new PacketReader();
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var addressEncoded = StandardCharsets.US_ASCII.encode(ipAddress);
            bb.putInt(ConnectionPacket.ID).putInt(addressEncoded.remaining()).put(addressEncoded).putInt(port);
            assertEquals(Reader.ProcessStatus.DONE,pr.process(bb));
            assertEquals(packet,pr.get());
        }

        @Test
        public void packetReaderTestRefill() {
            var ipAddress = "127.0.0.1";
            var port = 5000;
            var inetAddress = new InetSocketAddress(ipAddress, port);
            var packet = new ConnectionPacket(inetAddress);
            var pr = new PacketReader();
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var addressEncoded = StandardCharsets.US_ASCII.encode(ipAddress);
            bb.putInt(ConnectionPacket.ID).putInt(addressEncoded.remaining()).put(addressEncoded);
            assertEquals(Reader.ProcessStatus.REFILL,pr.process(bb));
            bb.putInt(port);
            assertEquals(Reader.ProcessStatus.DONE,pr.process(bb));
            assertEquals(packet,pr.get());
        }

        @Test
        public void packetReaderTestReset() {
            var ipAddress = "127.0.0.1";
            var port = 5000;
            var inetAddress = new InetSocketAddress(ipAddress, port);
            var packet = new ConnectionPacket(inetAddress);
            var pr = new PacketReader();
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var addressEncoded = StandardCharsets.US_ASCII.encode(ipAddress);

            //TODO RESETTTT
            bb.putInt(ConnectionPacket.ID).putInt(addressEncoded.remaining()).put(addressEncoded);
            assertEquals(Reader.ProcessStatus.REFILL,pr.process(bb));
            bb.putInt(port);
            assertEquals(Reader.ProcessStatus.DONE,pr.process(bb));
            assertEquals(packet,pr.get());
        }
    }


    public static class CalculationResultPacketReaderTest {
        @Test
        public void CalculationResultPacketReaderTestSimple() {
            // TODO split writer and reader part
            var bb = ByteBuffer.allocate(Context.CONTEXT_BUFFER_SIZE);
            var ip = "127.0.0.1";
            var port1 = 5000;
            var ad1 = new InetSocketAddress(ip,port1);
            var port2 = 6000;
            var ad2 = new InetSocketAddress(ip,port2);
            var calcId = 100L;
            var results = new ArrayList<String>();
            results.add("Helle");
            results.add("World");
            var p = new CalculationResultPacket(ad1,ad2,calcId,results);
            var pw = p.createWriter();
            assertEquals(Writer.ProcessStatus.DONE,pw.process(bb));

            bb.flip();
            // Check packet id
            assertEquals(CalculationResultPacket.ID,bb.getInt());

            // Check ip length
            var length = bb.getInt();
            assertEquals(ip.length(),length);

            // Check IP
            var originalLimit = bb.limit();
            bb.limit(bb.position() + length);
            var ipDecoded = StandardCharsets.US_ASCII.decode(bb).toString();
            bb.limit(originalLimit);
            assertEquals(ip,ipDecoded);

            // Check port 1
            assertEquals(port1,bb.getInt());

            // Check ip2 length
            length = bb.getInt();
            assertEquals(ip.length(),length);

            // Check IP2
            originalLimit = bb.limit();
            bb.limit(bb.position() + length);
            ipDecoded = StandardCharsets.US_ASCII.decode(bb).toString();
            bb.limit(originalLimit);
            assertEquals(ip,ipDecoded);

            // Check port 2
            assertEquals(port2,bb.getInt());


            // Calculation id
            assertEquals(calcId,bb.getLong());

            // Results count
            assertEquals(results.size(),bb.getLong());

            // Result 1 character count
            assertEquals(5,bb.getInt());

            //Result 1 bytes count
            assertEquals(5,bb.getInt());

            // Check result 1
            originalLimit = bb.limit();
            bb.limit(bb.position() + 5);
            var result1 = StandardCharsets.US_ASCII.decode(bb).toString();
            bb.limit(originalLimit);
            assertEquals(results.get(0),result1);

            // Result 2 character count
            assertEquals(5,bb.getInt());

            //Result 2 bytes count
            assertEquals(5,bb.getInt());

            // Check result 2
            originalLimit = bb.limit();
            bb.limit(bb.position() + 5);
            var result2 = StandardCharsets.US_ASCII.decode(bb).toString();
            bb.limit(originalLimit);
            assertEquals(results.get(1),result2);

            assertEquals(0,bb.remaining());

            var pr = new PacketReader();
            assertEquals(Reader.ProcessStatus.DONE,pr.process(bb));
            assertEquals(p,pr.get());
        }

    }
}
