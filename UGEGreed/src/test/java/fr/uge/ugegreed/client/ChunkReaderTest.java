package fr.uge.ugegreed.client;

import static org.junit.Assert.assertEquals;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import fr.uge.ugegreed.packet.reader.http.ChunkPartReader;
import fr.uge.ugegreed.packet.reader.http.ChunkReader;
import org.junit.jupiter.api.Test;

import fr.uge.ugegreed.packet.reader.Reader;

public class ChunkReaderTest {

  @Test
  void process() {
    var text = """
        4\r\n\
        Wiki\r\n\
        7\r\n\
        pedia i\r\n\
        B\r\n\
        n \r\nchunks.\r\n\
        0\r\n\
        \r\n\
        """;
    Charset UTF8 = StandardCharsets.UTF_8;
    var buffer = UTF8.encode(text);
    buffer.compact();

    var reader = new ChunkPartReader();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals(UTF8.encode("Wiki"), reader.get().flip());

    reader.reset();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals(UTF8.encode("pedia i"), reader.get().flip());

    reader.reset();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals(UTF8.encode("n \r\nchunks."), reader.get().flip());
  }

  @Test
  public void emptyChunk() {
    var text = """
        0\r\n\
        \r\n\
        """;
    Charset UTF8 = StandardCharsets.UTF_8;
    var buffer = UTF8.encode(text);
    buffer.compact();

    var reader = new ChunkReader();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals(UTF8.encode(""), reader.get().flip());

  }
}