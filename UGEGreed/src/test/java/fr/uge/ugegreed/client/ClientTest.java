package fr.uge.ugegreed.client;

import fr.uge.ugegreed.operation.*;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(Enclosed.class)
public class ClientTest {

    public static class CalculationWorkerTest {
        @Test
        public void calculationWorkerTestSimple() throws InterruptedException {
            var blockingQueue = new ArrayBlockingQueue<Operation>(10);
            var destAdr = new InetSocketAddress("127.0.0.1",5000);
            var worker = new CalculationWorker(blockingQueue::add);
            var calculation = new CalculationRequestOperation(
                    destAdr,
                    URI.create("http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar"),
                    "fr.uge.factors.Factorizer",
                    55,
                    10,
                    12);
            worker.addCalculation(calculation);
            var result = blockingQueue.take();
            var resultOp = (RemoteCalculationResultOperation) result;
            var calculable = (CalculationRequestOperation)resultOp.calculableOperation();
            assertEquals(calculable.responseDestinationAddress(), destAdr);
            assertEquals(calculable.calculationId(), 55);
            assertEquals(resultOp.responses().size(), 2);
            assertEquals(resultOp.responses().toString(), """
                    [{
                     taskId : 10,
                     factors : [1, 2, 5, 10]
                    }, {
                     taskId : 11,
                     factors : [1, 11]
                    }]"""
            );
        }

        @Test
        public void calculationWorkerTestLotOfCalculation() throws InterruptedException {
            var blockingQueue = new ArrayBlockingQueue<Operation>(1_000_000);
            var destAdr = new InetSocketAddress("127.0.0.1",5000);
            var count = 1000;
            var worker = new CalculationWorker(blockingQueue::add);
            var calculations = new ArrayList<CalculationRequestOperation>();
            IntStream.range(1,count).forEach(i -> {
                var calculation = new CalculationRequestOperation(
                        destAdr,
                        URI.create("http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar"),
                        "fr.uge.factors.Factorizer",
                        i,
                        1,
                        100);
                calculations.add(calculation);
                worker.addCalculation(calculation);
            });
            for(var __ : calculations){
                var result = blockingQueue.take();
                var resultOp = (RemoteCalculationResultOperation) result;
                var calculable = (CalculationRequestOperation)resultOp.calculableOperation();
                assertEquals(calculable.responseDestinationAddress(), destAdr);
                assertTrue(resultOp.calculationId() >= 1 && resultOp.calculationId() <= count);
                assertEquals(resultOp.responses().size(), 99);
            }
        }
    }
}
