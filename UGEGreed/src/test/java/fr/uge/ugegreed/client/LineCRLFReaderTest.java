package fr.uge.ugegreed.client;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import fr.uge.ugegreed.packet.reader.http.LineCRLFReader;
import org.junit.jupiter.api.Test;

import fr.uge.ugegreed.client.http.HTTPClient;
import fr.uge.ugegreed.packet.reader.Reader;

class LineCRLFReaderTest {

  @Test
  void process() {
    var text = "Debut\rSuite\n\rFin\n\r\nANEPASTOUCHER";

    var buffer = StandardCharsets.US_ASCII.encode(text);
    buffer.compact();

    var reader = new LineCRLFReader();
    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals("Debut\rSuite\n\rFin\n", reader.get());

    ByteBuffer buffFinal = StandardCharsets.US_ASCII.encode("ANEPASTOUCHER");
    assertEquals(buffFinal, buffer.flip());
  }

  @Test
  void process2() {
    var text = "Debut\r\nSuite\n\rFin\n\r\nANEPASTOUCHER";
    var buffer = StandardCharsets.US_ASCII.encode(text);
    buffer.compact();

    var reader = new LineCRLFReader();
    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals("Debut", reader.get());

    reader.reset();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));
    assertEquals("Suite\n\rFin\n", reader.get());

    ByteBuffer buffFinal = StandardCharsets.US_ASCII.encode("ANEPASTOUCHER");
    assertEquals(buffFinal, buffer.flip());
  }

  @Test
  void process3() {
    var text = "Debut\n";
    var buffer = StandardCharsets.US_ASCII.encode(text);
    buffer.compact();

    var reader = new LineCRLFReader();
    assertEquals(Reader.ProcessStatus.REFILL, reader.process(buffer));
    assertThrows(IllegalStateException.class, reader::get);

    var text2 = "Fin\r\n";
    var buffer2 = StandardCharsets.US_ASCII.encode(text2);
    buffer2.compact();

    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer2));
    assertEquals("Debut\nFin", reader.get());
  }
  
  @Test
  void process4() throws IOException, InterruptedException {
    var url = new URL("http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar");
    var client = new HTTPClient(url);
    
    client.launch();
    client.queueCommand(url);
    Thread.sleep(10000);
    
//    var text = "Debut\n";
//    var buffer = StandardCharsets.US_ASCII.encode(text);
//    buffer.compact();
//
//    var reader = new LineCRLFReader();
//    assertEquals(Reader.ProcessStatus.REFILL, reader.process(buffer));
//    assertThrows(IllegalStateException.class, reader::get);
//
//    var text2 = "Fin\r\n";
//    var buffer2 = StandardCharsets.US_ASCII.encode(text2);
//    buffer2.compact();
//
//    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer2));
//    assertEquals("Debut\nFin", reader.get());
  }
  
  @Test
  void process5() {
    var text = "\r\n";
    var buffer = StandardCharsets.US_ASCII.encode(text);
    buffer.compact();

    var reader = new LineCRLFReader();
    assertEquals(Reader.ProcessStatus.DONE, reader.process(buffer));

  }
}