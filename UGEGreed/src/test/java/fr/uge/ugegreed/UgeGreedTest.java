package fr.uge.ugegreed;

import fr.uge.ugegreed.network.Context;
import fr.uge.ugegreed.network.NetworkManager;
import fr.uge.ugegreed.operation.CalculationRequestOperation;
import fr.uge.ugegreed.operation.DisconnectOperation;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UgeGreedTest {
    private static final long APPLICATION_START_DELAY = 500;
    private static final long APPLICATION_CALCULATION_DELAY = 5000;
    private static final Path testFilePath = Path.of("test.txt");
    private static final Path testFilePath2 = Path.of("test2.txt");
    private static final Path testFilePath3 = Path.of("test3.txt");

    private static void cleanTest() throws IOException {
        if(Files.exists(testFilePath)){
            Files.delete(testFilePath);
        }
        if(Files.exists(testFilePath2)){
            Files.delete(testFilePath2);
        }
        if(Files.exists(testFilePath3)){
            Files.delete(testFilePath3);
        }
    }

    @Test
    public void networkCalculationSingleClient() throws IOException, InterruptedException {
        cleanTest();
        var rootPort = 55555;
        var path = Path.of("");
        var rootAddress = new InetSocketAddress("127.0.0.1", rootPort); //"192.168.0.14" //"0:0:0:0:0:0:0:0"
        System.out.println("Root started on " + rootAddress);
        var root = UGEGreed.createRootApplication(rootPort, path);

        Thread.sleep(APPLICATION_START_DELAY);

        var operationArgs = new String[]{"COMMAND", "http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar", "fr.uge.factors.Factorizer", "1", "1000", "test.txt"};
        root.createCalculation(operationArgs);

        Thread.sleep(APPLICATION_CALCULATION_DELAY);

        var lines = Files.readAllLines(testFilePath);
        var taskCount = 0;
        for (var line : lines){
            if (line.contains("taskId")) {
                taskCount++;
            }
        }
        assertEquals(999, taskCount);
        cleanTest();
    }

    @Test
    public void networkCalculationMultipleClient() throws IOException, InterruptedException {
        cleanTest();
        var rootPort = 55555;
        var path = Path.of("");
        var rootAddress = new InetSocketAddress("127.0.0.1", rootPort); //"192.168.0.14" //"0:0:0:0:0:0:0:0"
        System.out.println("Root started on " + rootAddress);
        var rootThread = Thread.ofPlatform().start(() -> {
            try {
                var root = UGEGreed.createRootApplication(rootPort, path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        Thread.sleep(APPLICATION_START_DELAY);

        // Start C1 -> ROOT
        var c1Port = 2222;
        var c1Thread = Thread.ofPlatform().start(() -> {
            try {
                var c1 = UGEGreed.createClientApplication(c1Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C2 -> ROOT
        var c2Port = 3333;
        var c2Thread = Thread.ofPlatform().start(() -> {
            try {
                var c2 = UGEGreed.createClientApplication(c2Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });


        Thread.sleep(APPLICATION_START_DELAY);

        // Start C3 -> C2
        var c3Port = 4444;
        var c3Thread = Thread.ofPlatform().start(() -> {
            try {
                var c3 = UGEGreed.createClientApplication(c3Port, path, new InetSocketAddress("127.0.0.1",c2Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C4 -> C3
        var c4Port = 6666;
        var c4Thread = Thread.ofPlatform().start(() -> {
            try {
                var c4 = UGEGreed.createClientApplication(c4Port, path, new InetSocketAddress("127.0.0.1",c3Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C5 -> C4
        var c5Port = 7777;
        var c5 = UGEGreed.createClientApplication(c5Port, path, new InetSocketAddress("127.0.0.1",c4Port));

        Thread.sleep(APPLICATION_START_DELAY);

        System.out.println("All clients connected to the network");

        var operationArgs = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar", "fr.uge.factors.Factorizer","1","13","test.txt"};
        c5.createCalculation(operationArgs);

        Thread.sleep(APPLICATION_CALCULATION_DELAY);


        var lines = Files.readAllLines(testFilePath);
        var taskCount = 0;
        for (var line : lines){
            if (line.contains("taskId")) {
                taskCount++;
            }
        }
        assertEquals(12, taskCount);
        cleanTest();
    }

    @Test
    public void networkCalculationMultipleClientAndMultipleKindOfCalculation() throws IOException, InterruptedException {
        cleanTest();
        var rootPort = 55555;
        var path = Path.of("");
        var rootAddress = new InetSocketAddress("127.0.0.1", rootPort); //"192.168.0.14" //"0:0:0:0:0:0:0:0"
        System.out.println("Root started on " + rootAddress);
        var rootThread = Thread.ofPlatform().start(() -> {
            try {
                var root = UGEGreed.createRootApplication(rootPort, path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        Thread.sleep(APPLICATION_START_DELAY);

        // Start C1 -> ROOT
        var c1Port = 2222;
        var c1Thread = Thread.ofPlatform().start(() -> {
            try {
                var c1 = UGEGreed.createClientApplication(c1Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C2 -> ROOT
        var c2Port = 3333;
        var c2Thread = Thread.ofPlatform().start(() -> {
            try {
                var c2 = UGEGreed.createClientApplication(c2Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });


        Thread.sleep(APPLICATION_START_DELAY);

        // Start C3 -> C2
        var c3Port = 4444;
        var c3Thread = Thread.ofPlatform().start(() -> {
            try {
                var c3 = UGEGreed.createClientApplication(c3Port, path, new InetSocketAddress("127.0.0.1",c2Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C4 -> C3
        var c4Port = 6666;
        var c4Thread = Thread.ofPlatform().start(() -> {
            try {
                var c4 = UGEGreed.createClientApplication(c4Port, path, new InetSocketAddress("127.0.0.1",c3Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C5 -> C4
        var c5Port = 7777;
        var c5 = UGEGreed.createClientApplication(c5Port, path, new InetSocketAddress("127.0.0.1",c4Port));

        Thread.sleep(APPLICATION_START_DELAY);

        System.out.println("All clients connected to the network");

        var operation1Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar", "fr.uge.factors.Factorizer","1","10","test.txt"};
        var operation2Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/Collatz.jar", "fr.uge.collatz.Collatz","1","10","test2.txt"};
        var operation3Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/SlowChecker.jar", "fr.uge.slow.SlowChecker","1","10","test3.txt"};
        c5.createCalculation(operation1Args);
        c5.createCalculation(operation2Args);
        c5.createCalculation(operation3Args);

        Thread.sleep(3*APPLICATION_CALCULATION_DELAY);

        var lines = Files.readAllLines(testFilePath);
        lines.addAll(Files.readAllLines(testFilePath2));
        lines.addAll(Files.readAllLines(testFilePath3));
        var taskCount = 0;
        for (var line : lines){
            if (line.contains("taskId")) {
                taskCount++;
            }
        }
        assertEquals(27, taskCount);
        cleanTest();
    }
    @Test
    public void networkCalculationMultipleClientFromRoot() throws IOException, InterruptedException {
        cleanTest();
        var rootPort = 55555;
        var path = Path.of("");
        var rootAddress = new InetSocketAddress("127.0.0.1", rootPort); //"192.168.0.14" //"0:0:0:0:0:0:0:0"
        System.out.println("Root started on " + rootAddress);
        var rootThread = Thread.ofPlatform().start(() -> {
            try {
                var root = UGEGreed.createRootApplication(rootPort, path);
                Thread.sleep(7 * APPLICATION_START_DELAY);
                var operation1Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar", "fr.uge.factors.Factorizer","1","10","test.txt"};
                var operation2Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/Collatz.jar", "fr.uge.collatz.Collatz","1","10","test2.txt"};
                var operation3Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/SlowChecker.jar", "fr.uge.slow.SlowChecker","1","10","test3.txt"};
                root.createCalculation(operation1Args);
                root.createCalculation(operation2Args);
                root.createCalculation(operation3Args);

            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        Thread.sleep(APPLICATION_START_DELAY);

        // Start C1 -> ROOT
        var c1Port = 2222;
        var c1Thread = Thread.ofPlatform().start(() -> {
            try {
                var c1 = UGEGreed.createClientApplication(c1Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C2 -> ROOT
        var c2Port = 3333;
        var c2Thread = Thread.ofPlatform().start(() -> {
            try {
                var c2 = UGEGreed.createClientApplication(c2Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });


        Thread.sleep(APPLICATION_START_DELAY);

        // Start C3 -> C2
        var c3Port = 4444;
        var c3Thread = Thread.ofPlatform().start(() -> {
            try {
                var c3 = UGEGreed.createClientApplication(c3Port, path, new InetSocketAddress("127.0.0.1",c2Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C4 -> C3
        var c4Port = 6666;
        var c4Thread = Thread.ofPlatform().start(() -> {
            try {
                var c4 = UGEGreed.createClientApplication(c4Port, path, new InetSocketAddress("127.0.0.1",c3Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C5 -> C4
        var c5Port = 7777;
        var c5 = UGEGreed.createClientApplication(c5Port, path, new InetSocketAddress("127.0.0.1",c4Port));

        Thread.sleep(APPLICATION_START_DELAY);

        System.out.println("All clients connected to the network");

        Thread.sleep(3*APPLICATION_CALCULATION_DELAY);

        var lines = Files.readAllLines(testFilePath);
        lines.addAll(Files.readAllLines(testFilePath2));
        lines.addAll(Files.readAllLines(testFilePath3));
        var taskCount = 0;
        for (var line : lines){
            if (line.contains("taskId")) {
                taskCount++;
            }
        }
        assertEquals(27, taskCount);
        cleanTest();

    }

    @Test
    public void networkCalculationMultipleClientBetweenChildAndRoot() throws IOException, InterruptedException {
        cleanTest();
        var rootPort = 55555;
        var path = Path.of("");
        var rootAddress = new InetSocketAddress("127.0.0.1", rootPort); //"192.168.0.14" //"0:0:0:0:0:0:0:0"
        System.out.println("Root started on " + rootAddress);
        var rootThread = Thread.ofPlatform().start(() -> {
            try {
                var root = UGEGreed.createRootApplication(rootPort, path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        Thread.sleep(APPLICATION_START_DELAY);

        // Start C1 -> ROOT
        var c1Port = 2222;
        var c1Thread = Thread.ofPlatform().start(() -> {
            try {
                var c1 = UGEGreed.createClientApplication(c1Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C2 -> ROOT
        var c2Port = 3333;
        var c2Thread = Thread.ofPlatform().start(() -> {
            try {
                var c2 = UGEGreed.createClientApplication(c2Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });


        Thread.sleep(APPLICATION_START_DELAY);

        // Start C3 -> C2
        var c3Port = 4444;
        var c3Thread = Thread.ofPlatform().start(() -> {
            try {
                var c3 = UGEGreed.createClientApplication(c3Port, path, new InetSocketAddress("127.0.0.1",c2Port));
                Thread.sleep(7 * APPLICATION_START_DELAY);
                var operation1Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/Factorizer.jar", "fr.uge.factors.Factorizer","1","9","test.txt"};
                var operation2Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/Collatz.jar", "fr.uge.collatz.Collatz","1","9","test2.txt"};
                var operation3Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/SlowChecker.jar", "fr.uge.slow.SlowChecker","1","9","test3.txt"};
                c3.createCalculation(operation1Args);
                c3.createCalculation(operation2Args);
                c3.createCalculation(operation3Args);
            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C4 -> C3
        var c4Port = 6666;
        var c4Thread = Thread.ofPlatform().start(() -> {
            try {
                var c4 = UGEGreed.createClientApplication(c4Port, path, new InetSocketAddress("127.0.0.1",c3Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C5 -> C4
        var c5Port = 7777;
        var c5 = UGEGreed.createClientApplication(c5Port, path, new InetSocketAddress("127.0.0.1",c4Port));

        Thread.sleep(APPLICATION_START_DELAY);

        System.out.println("All clients connected to the network");

        Thread.sleep(3*APPLICATION_CALCULATION_DELAY);

        var lines = Files.readAllLines(testFilePath);
        lines.addAll(Files.readAllLines(testFilePath2));
        lines.addAll(Files.readAllLines(testFilePath3));
        var taskCount = 0;
        for (var line : lines){
            if (line.contains("taskId")) {
                taskCount++;
            }
        }
        assertEquals(24, taskCount);
        cleanTest();

    }



    @Test
    public void networkDisconnection() throws IOException, InterruptedException {
        var rootPort = 55555;
        var path = Path.of("");
        var rootAddress = new InetSocketAddress("127.0.0.1", rootPort); //"192.168.0.14" //"0:0:0:0:0:0:0:0"
        System.out.println("Root started on " + rootAddress);
        var rootThread = Thread.ofPlatform().start(() -> {
            try {
                var root = UGEGreed.createRootApplication(rootPort, path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        Thread.sleep(APPLICATION_START_DELAY);

        // Start C1 -> ROOT
        var c1Port = 2222;
        var c1Thread = Thread.ofPlatform().start(() -> {
            try {
                var c1 = UGEGreed.createClientApplication(c1Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C2 -> ROOT
        var c2Port = 3333;
        var c2Thread = Thread.ofPlatform().start(() -> {
            try {
                var c2 = UGEGreed.createClientApplication(c2Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });


        Thread.sleep(APPLICATION_START_DELAY);

        // Start C3 -> C2
        var c3Port = 4444;
        var c3Thread = Thread.ofPlatform().start(() -> {
            try {
                var c3 = UGEGreed.createClientApplication(c3Port, path, new InetSocketAddress("127.0.0.1",c2Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C4 -> C3
        var c4Port = 6666;
        var c4Thread = Thread.ofPlatform().start(() -> {
            try {
                var c4 = UGEGreed.createClientApplication(c4Port, path, new InetSocketAddress("127.0.0.1",c3Port));
                Thread.sleep(3*APPLICATION_START_DELAY);
                c4.sendOperation(new DisconnectOperation());
            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C5 -> C4
        var c5Port = 7777;
        var c5 = UGEGreed.createClientApplication(c5Port, path, new InetSocketAddress("127.0.0.1",c4Port));

        Thread.sleep(APPLICATION_START_DELAY);

        Thread.sleep(20_000);
        assertEquals(12, 12);
    }

    @Test
    public void networkInterruptAndDispatchCalculation() throws IOException, InterruptedException {
        cleanTest();
        var rootPort = 55555;
        var path = Path.of("");
        var rootAddress = new InetSocketAddress("127.0.0.1", rootPort); //"192.168.0.14" //"0:0:0:0:0:0:0:0"
        System.out.println("Root started on " + rootAddress);
        var rootThread = Thread.ofPlatform().start(() -> {
            try {
                var root = UGEGreed.createRootApplication(rootPort, path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        Thread.sleep(APPLICATION_START_DELAY);

        // Start C1 -> ROOT
        var c1Port = 2222;
        var c1Thread = Thread.ofPlatform().start(() -> {
            try {
                var c1 = UGEGreed.createClientApplication(c1Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C2 -> ROOT
        var c2Port = 3333;
        var c2Thread = Thread.ofPlatform().start(() -> {
            try {
                var c2 = UGEGreed.createClientApplication(c2Port, path, rootAddress);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });


        Thread.sleep(APPLICATION_START_DELAY);

        // Start C3 -> C2
        var c3Port = 4444;
        var c3Thread = Thread.ofPlatform().start(() -> {
            try {
                var c3 = UGEGreed.createClientApplication(c3Port, path, new InetSocketAddress("127.0.0.1",c2Port));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C4 -> C3
        var c4Port = 6666;
        var c4Thread = Thread.ofPlatform().start(() -> {
            try {
                var c4 = UGEGreed.createClientApplication(c4Port, path, new InetSocketAddress("127.0.0.1",c3Port));
                var operation1Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/SlowChecker.jar", "fr.uge.slow.SlowChecker","1","50","test.txt"};
                Thread.sleep(APPLICATION_CALCULATION_DELAY);
                c4.createCalculation(operation1Args);
                Thread.sleep(2*APPLICATION_CALCULATION_DELAY);
                c4.sendOperation(new DisconnectOperation());
            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread.sleep(APPLICATION_START_DELAY);

        // Start C5 -> C4
        var c5Port = 7777;
        var c5 = UGEGreed.createClientApplication(c5Port, path, new InetSocketAddress("127.0.0.1",c4Port));

        Thread.sleep(APPLICATION_START_DELAY);

        var operation2Args = new String[]{"COMMAND","http://www-igm.univ-mlv.fr/~carayol/SlowChecker.jar", "fr.uge.slow.SlowChecker","1","50","test2.txt"};
        c5.createCalculation(operation2Args);
        Thread.sleep(30_000);
        //cleanTest();
    }
}
